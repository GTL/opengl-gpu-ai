#pragma once
#if defined(_WIN32)
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#endif

#include <Cross/Core.h>
#include <Cross/Math/Vector2.h>
struct Vector3;

namespace Cross
{
	class Window;

	class Input
	{
	public:
		struct Key
		{
			static Key a; static Key b; static Key c; static Key d;
			static Key e; static Key f; static Key g; static Key h;
			static Key i; static Key j; static Key k; static Key l;
			static Key m; static Key n; static Key o; static Key p;
			static Key q; static Key r; static Key s; static Key t;
			static Key u; static Key v; static Key w; static Key x;
			static Key y; static Key z;
			static Key space;
			static Key leftShift;
			static Key up, down, left, right;

			Key(UByte key);

			UByte key;
		};
	private:
		const Window* window;

#if defined(_WIN32)
		LPDIRECTINPUT8 object;
		LPDIRECTINPUTDEVICE8 keyboardDevice;
		LPDIRECTINPUTDEVICE8 mouseDevice;

		UByte keyboardState[256];
		DIMOUSESTATE2 mouseState;

		UByte prevKeyboardState[256];
		DIMOUSESTATE2 prevMouseState;
#elif defined(unix)
		Bool keyboardState[256];
		Bool prevKeyboardState[256];
		Vector2 prevMousePosition;
#endif

		Vector2 mousePosition;

		void calculateMousePosition();
	public:
		Input(Cross::Window* window);
		~Input();

		bool keyDown(Key key) const;
		bool keyPressed(Key key) const;
		bool keyReleased(Key key) const;

		bool mouseDown(UByte button) const;
		bool mousePressed(UByte button) const;
		bool mouseReleased(UByte button) const;

		void update();

		Vector3 getMouseMove() const;
		Vector2 getMousePosition() const;

		void setMousePosition(const Vector2& mousePosition);

		void setBackground(bool background);
	};
}
