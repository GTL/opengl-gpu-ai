#pragma once
#include <Cross/Core.h>
#include <string>
#include <sstream>

namespace Cross
{
	template <class T> T build(UByte* data);
};

template <class T> T Cross::build(UByte* data)
{
	T ret;
	for (UInt i = 0; i < sizeof(T); i++)
	{
		((UByte*)&ret)[i] = data[i];
	}
	return ret;
}

template <class T> T stringTo(const std::string& string)
{
	std::stringstream ss(std::stringstream::in | std::stringstream::out);
	ss << string;
	ss.seekp(ss.beg);
	T ret;
	ss >> ret;
	return ret;
}