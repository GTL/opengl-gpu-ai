#pragma once
#include <Cross/Core.h>
#include <string>

namespace Cross
{
	class File
	{
	public:
		static bool getText(const std::string& fileName, std::string& result);
		static bool getData(const std::string& fileName, UByte** data, UInt* size = nullptr);
	};
}