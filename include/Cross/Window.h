#pragma once
#if defined (_WIN32)
#include <Windows.h>
#endif
#include <string>
#include <functional>
#include <map>
#include <vector>
#include <Cross/Graphics/Color.h>
struct Vector2;

namespace Cross
{
	class Window
	{
	public:
		struct Class
		{
			std::string name;
			unsigned int width;
			unsigned int height;
			Color background;
		};
	private:
#if defined(_WIN32)
		static std::map<HWND, Window*> handleMap;

		HWND handle;
		DWORD style;
		DWORD extendedStyle;

		static LRESULT CALLBACK winProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam);
#elif defined(unix)
		::Window window;
		Display* display;
		XVisualInfo* visual;
		GLXFBConfig* config;
		XSetWindowAttributes windowAttributes;
		GLXContext context;
		std::vector<XEvent> events;
#endif

		Window(const Window& copy);
	public:
		Window(const Class& windowClass);
		virtual ~Window();

#if defined(_WIN32)
		HWND getHandle() const;
		DWORD getStyle() const;
#elif defined(unix)
		::Window getWindow() const;
		Display* getDisplay() const;
		GLXContext getContext() const;
		std::vector<XEvent> getEvents() const;
#endif

		void update();

		void show() const;
		void hide() const;

		void focus() const;

		void setWidth(const unsigned int width) const;
		void setHeight(const unsigned int height) const;

		unsigned int getWidth() const;
		unsigned int getHeight() const;

		Vector2 getPosition() const;

		void setTitle(const std::string& title) const;
		std::string getTitle() const;

		bool isDestroyed();

		//void onFocus(const std::function<void()>& function);
	};
}
