#pragma once
#include <string>

struct Exception
{
	std::string message;
	std::string file;
	long line;
	int result;
	Exception(std::string message, std::string file, long line);
	Exception(std::string message, std::string file, long line, int result);
};

#define HR(x)\
{\
	int hr = int(x);\
	if (FAILED(hr))\
	{\
		throw Exception(#x, __FILE__, __LINE__, hr);\
	}\
}

#define Ex(message) throw Exception(message, __FILE__, __LINE__)
