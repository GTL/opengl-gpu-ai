#pragma once

namespace Cross
{
	class Window;
	class Graphics;
	class Input;

	class Application
	{
		static bool running;
	private:
		float previousTime;
	protected:
		Window* window;
		Graphics* graphics;
		Input* input;
	public:
		static void quit();
		static void sleep(float milliseconds);

		Application();
		virtual ~Application();

		virtual void initialize();
		virtual void update(const float dt);
		virtual void draw();
		virtual void uninitialize();

		void run();
	};
}
