#pragma once
#if defined(_WIN32)
#include <Windows.h>
#include <GL/glew.h>
#include <GL/gl.h>
#elif defined(unix)
#include <X11/Xlib.h>
#include <GL/glew.h>
#include <GL/glxew.h>
#include <GL/gl.h>
//#include <GL/glx.h>
#endif

#define freeptr(ptr) if (ptr) { delete ptr; ptr = nullptr; }

// boolean
//typedef GLboolean Bool;

// character type
typedef char Char;

// integer types
typedef GLbyte Byte;
typedef GLubyte UByte;
typedef GLshort Short;
typedef GLushort UShort;
typedef GLint Int;
typedef GLuint UInt;
typedef long int Long;
typedef unsigned long int ULong;

// integer type with sizes
typedef Byte Int8;
typedef UByte UInt8;
typedef Short Int16;
typedef UShort UInt16;
typedef Int Int32;
typedef UInt UInt32;
typedef Long Int64;
typedef ULong UInt64;

// float point numbers
typedef GLfloat Float;
typedef GLdouble Double;
