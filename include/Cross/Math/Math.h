#pragma once
#undef min
#undef max

namespace Math
{
	const float pi = 3.14159f;
	const float piOverTwo = pi / 2.0f;

	float sign(float x);
	float min(float a, float b);
	float max(float a, float b);
	float floor(float x);
	float ceil(float x);
	float round(float x);
	float cos(float x);
	float sin(float x);
	float tan(float x);
	float abs(float x);
	float squared(float x);
	float sqrt(float x);
	float atan2(float y, float x);
	float degToRad(float degree);
	float radToDeg(float radian);
};