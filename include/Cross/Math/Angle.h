#pragma once
#include "../Math/Math.h"
struct Vector2;

class Angle
{
	float radians;
public:
	static Angle inverse(const Angle& angle);
	static Angle difference(const Angle& a, const Angle& b);
	static Angle center(const Angle& a, const Angle& b);
	static Angle inverseCenter(const Angle& a, const Angle& b);

	Angle();
	Angle(float radians);
	Angle(const Vector2& direction);

	void setRadians(float radians);
	float getRadians() const;
	float getRadiansPositive() const;

	void setDegrees(float degrees);
	float getDegrees() const;
	float getDegreesPositive() const;

	void setDirection(const Vector2& direction);
	Vector2 getDirection() const;

	void constrain(Angle from, Angle to);
};