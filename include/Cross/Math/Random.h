#pragma once
#include <string>

class Random
{
	unsigned int multiplier;
	unsigned int addition;
	unsigned int modulus;
	unsigned int seed;
public:
	static float uniformDist(float a, float b, float u);
	static float exponentialDist(float beta, float u);
	static float weibullDist(float a, float b, float c, float u);
	static float triangularDist(float xmin, float xmax, float c, float u);
	static float normal(float mean, float variance, float x);

	Random();
	Random(const unsigned int seed);
	int getInteger(int min, int max);
	float getDecimal();
	std::string getKey(const std::string& chars, const int length);

	float getUniform(float a, float b);
	float getExponential(float beta);
	float getWeibull(float a, float b, float c);
	float getNormal(float mean, float variance);
};