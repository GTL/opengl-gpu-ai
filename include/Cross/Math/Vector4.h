#pragma once
struct Vector2;
struct Vector3;
struct Matrix;

struct Vector4
{
	union
	{
		struct
		{
			float x, y, z, w;
		};
		float m[4];
	};

	static const Vector4 zero;
	static const Vector4 one;

	static float distance(const Vector4& vectorA, const Vector4& vectorB);
	static float distanceSquared(const Vector4& vectorA, const Vector4& vectorB);
	static float distanceManhattan(const Vector4& vectorA, const Vector4& vectorB);
	static Vector4 normalized(const Vector4& vector);
	static float dotProduct(const Vector4& vectorA, const Vector4& vectorB);

	Vector4();
	Vector4(const Vector4& copy);
	Vector4(float x, float y, float z, float w);
	Vector4(Vector2& vector, float z, float w);
	Vector4(Vector3& vector, float w);

	operator Vector2() const;
	operator Vector3() const;

	Vector4& operator=(const Vector4& operand);
	float& operator[](unsigned int index);
	const float operator[](unsigned int index) const;
	Vector4 operator*(const float operand) const;
	Vector4& operator*=(const float operand);
	Vector4 operator*(const Matrix& operand) const;
	Vector4& operator*=(const Matrix& operand);
	Vector4 operator/(const float operand) const;
	Vector4& operator/=(const float operand);
	Vector4 operator+(const Vector4& operand) const;
	Vector4& operator+=(const Vector4& operand);
	Vector4 operator-() const;
	Vector4 operator-(const Vector4& operand) const;
	Vector4& operator-=(const Vector4& operand);
	bool operator==(const Vector4& operand) const;
	bool operator!=(const Vector4& operand) const;

	void set(const float x, const float y, const float z, const float w);
	void clear();
	float getLength() const;
	float getLengthSquared() const;
	float getLengthManhattan() const;
	void normalize();
};