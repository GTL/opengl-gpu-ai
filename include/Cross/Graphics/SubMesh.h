#pragma once
#include <Cross/Core.h>
#include <Cross/Math/Vector2.h>
#include <Cross/Math/Vector3.h>
#include <Cross/Math/Vector4.h>
#include <Cross/Graphics/Color.h>
#include <map>

namespace Cross
{
	class SubMesh
	{
		UInt vertexArray;
		UInt vertexObjects;

		UInt vertices;

		bool quads;
	public:
		SubMesh(UInt vertexCount);

		UInt addBuffer(Vector2* positions, UInt id = ~0);
		UInt addBuffer(Vector3* positions, UInt id = ~0);
		UInt addBuffer(Vector4* positions, UInt id = ~0);
		UInt addBuffer(Color* colors, UInt id = ~0);

		void setQuads(bool quads);
		bool getQuads();

		void draw();
	};
}