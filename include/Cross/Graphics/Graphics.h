#pragma once

namespace Cross
{
	class Window;
	struct Color;
	struct VertexDeclaration;

	class Graphics
	{
	public:
		struct Parameters
		{
			unsigned int width, height;
			Window* window;
		};

		enum PrimitiveType
		{
			POINTLIST = 1, LINELIST, LINESTRIP, TRIANGLELIST, TRIANGLESTRIP, TRIANGLEFAN
		};
	private:
		Parameters parameters;

#if defined(_WIN32)
		PIXELFORMATDESCRIPTOR pfd;
		HDC deviceContext;
		UInt pixelFormat;
		HGLRC renderContext;
#elif defined(unix)
#endif

		Graphics(const Graphics& copy);

		void onLostDevice() const;
		void onResetDevice() const;
	public:
		Graphics(const Parameters& parameters);
		~Graphics();

		Parameters getParameters() const;

		void setWidth(unsigned int width);
		void setHeight(unsigned int height);

		bool testDevice() const;
		void resetDevice() const;

		void begin() const;
		void end() const;
		void present() const;

		void clear() const;
		void clear(const Color& color) const;

		//void drawPrimitive(PrimitiveType type, const VertexDeclaration& declaration, void* vertexData, const unsigned int primitiveCount) const;
	};
}
