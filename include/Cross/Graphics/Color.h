#pragma once
#include <Cross/Core.h>

namespace Cross
{
	struct Color
	{
		UByte r, g, b, a;

		static Color makeHSV(const int hue, const int saturation, const int value);

		Color();
		Color(UByte r, UByte g, UByte b);
		Color(UByte r, UByte g, UByte b, UByte a);
	};
}
