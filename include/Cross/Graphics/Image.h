#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Color.h>

namespace Cross
{
	class Image
	{
	protected:
		Cross::Color* data;

		UInt width, height;
	public:
		Image();
		~Image();

		void getData(Cross::Color* data);
		void setData(Cross::Color* data);

		UInt getWidth();
		UInt getHeight();
	};
}