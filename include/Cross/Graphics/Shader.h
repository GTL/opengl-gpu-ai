#pragma once
#include <string>
#include <map>
#include <Cross/Core.h>
#include <Cross/Math/Matrix.h>
#include <Cross/Graphics/Texture.h>

namespace Cross
{
	class Shader
	{
		int shaderID;
		int vertexID;
		int fragmentID;
		int textureID;
		std::map<std::string, int> textures;
	public:
		Shader(std::string vertex, std::string fragment);
		~Shader();

		void begin();
		void end();

		void setMatrix(const std::string& name, const Matrix& matrix);
		void setTexture(const std::string& name, Texture* texture);
		void setTexture(const std::string& name, UInt texture);

		void bindAttribute(int index, const std::string& name);

		int getID();
	};
}
