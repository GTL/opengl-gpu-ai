#pragma once
#include <Cross/Core.h>

namespace Cross
{
	class RenderTarget
	{
		union Viewport { struct { Int x, y; GLsizei width, height; }; float vp[4]; };
		static Viewport viewport;
		UInt texture, frameBuffer, renderBuffer, width, height;
	public:
		RenderTarget(UInt width, UInt height, UInt attachment = 0);
		~RenderTarget();

		UInt getTexture();

		void begin();
		void end();
	};
}