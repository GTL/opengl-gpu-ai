#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Image.h>
#include <string>

namespace Cross
{
	class Bitmap : public Image
	{
	public:
		Bitmap(const std::string& fileName);
	};
}