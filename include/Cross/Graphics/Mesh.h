#pragma once
#include <string>
#include <Cross/Core.h>
#include <Cross/Graphics/SubMesh.h>

namespace Cross
{
	class Mesh
	{
		std::vector<SubMesh*> subMeshes;
	public:
		static Mesh* makeBox();
		static Mesh* makeSkyBox();

		static Mesh* loadObj(const std::string& fileName);

		Mesh();

		void addSubMesh(SubMesh* subMesh);

		SubMesh* getSubMesh(UInt id);

		UInt getSubMeshCount();

		void draw();
	};
}
