#pragma once
#include <Cross/Core.h>
#include <string>

namespace Cross
{
	class Texture
	{
		UInt texture;
	public:
		Texture(const std::string& fileName);
		~Texture();

		UInt getTexture();
	};
}