#include <Influence Maps/InfluenceAppCPU.h>
#include <Cross/Utility/Time.h>
#include <Cross/Graphics/Bitmap.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
using namespace std;
using Cross::Input;

void InfluenceAppCPU::initialize()
{
	srand((unsigned int)time(0));

	// initialize the application
	Application::initialize();

	// set the window title
	window->setTitle("Cross Framework - Influence Map");

	// create the camera
	camera = new Camera(input);

	// load and compile the shaders
	shaderSky = new Cross::Shader("shaders/cubemap.vp", "shaders/cubemap.fp");
	shaderObjects = new Cross::Shader("shaders/lighting.vp", "shaders/lighting.fp");
	shaderGround = shaderObjects;
	shaderScreen = new Cross::Shader("shaders/screen.vp", "shaders/screen.fp");
	shaderInfluence = new Cross::Shader("shaders/influence.vp", "shaders/influence.fp");
	shaderComboInfluence = new Cross::Shader("shaders/comboInfluence.vp", "shaders/comboInfluence.fp");

	// initialize the shaders
	shaderSky->bindAttribute(0, "in_Position");
	shaderSky->bindAttribute(1, "in_TexCoords");
	shaderObjects->bindAttribute(0, "in_Position");
	shaderObjects->bindAttribute(1, "in_TexCoords");
	shaderScreen->bindAttribute(0, "in_Position");
	shaderScreen->bindAttribute(1, "in_TexCoords");
	shaderInfluence->bindAttribute(0, "in_Position");
	shaderInfluence->bindAttribute(1, "in_TexCoords");
	shaderComboInfluence->bindAttribute(0, "in_Position");
	shaderComboInfluence->bindAttribute(1, "in_TexCoords");

	// load in our textures
	textureAlly = new Cross::Texture("smile.bmp");
	textureEnemy = new Cross::Texture("bolt.bmp");
	textureGround = new Cross::Texture("checkers.bmp");
	textureFalloff = new Cross::Texture("alpha.bmp");
	textureFalloffCover = new Cross::Texture("alpha2.bmp");

	// create the mesh objects
	meshSkybox = Cross::Mesh::loadObj("skybox.obj");
	//meshBox = Cross::Mesh::makeBox();
	meshBox = Cross::Mesh::loadObj("box2.obj");
	meshSphere = Cross::Mesh::loadObj("sphere.obj");
	meshSprite = new Cross::Mesh();
	//Vector2 pos3[] = { Vector2(-1, -1), Vector2(1, -1), Vector2(1, 1), Vector2(-1, 1) };
	//Vector2 tex3[] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	Vector2 pos3[] = { Vector2(-1, 1), Vector2(1, 1), Vector2(1, -1), Vector2(-1, -1) };
	Vector2 tex3[] = { Vector2(0, 1), Vector2(1, 1), Vector2(1, 0), Vector2(0, 0) };
	meshSprite->addSubMesh(new Cross::SubMesh(4));
	meshSprite->getSubMesh(0)->setQuads(true);
	meshSprite->getSubMesh(0)->addBuffer(pos3);
	meshSprite->getSubMesh(0)->addBuffer(tex3);

	// create some enemies and allies for funzies
	for (int i = 0; i < 50; i++)
	{
		switch (rand() % 3)
		{
		case 0:
			entities.push_back(new Enemy(Vector3(float((rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			enemies.push_back((Enemy*)entities[entities.size() - 1]);
			break;
		case 1:
			entities.push_back(new Ally(Vector3((float(rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			allies.push_back((Ally*)entities[entities.size() - 1]);
			break;
		case 2:
			entities.push_back(new Cover(Vector3(float((rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			covers.push_back((Cover*)entities[entities.size() - 1]);
			break;
		}
	}

	// read in settings from settings.txt
	std::ifstream file;
	file.open("data/settings.txt");
	if (!file.is_open())
	{
		MessageBox(window->getHandle(), "Failed to load data/settings.txt", "Error", MB_OK);
		quit();
		return;
	}
	file >> mapWidth;
	file >> mapHeight;
	file >> influenceAmount;
	file >> falloffAmount;
	
	// create the render targets
	influence = new Cross::RenderTarget(mapWidth, mapHeight);
	influenceMapDanger = new short*[mapWidth];
	influenceMapCover = new short*[mapWidth];
	influenceMapCombo = new short*[mapWidth];
	for (int i = 0; i < mapWidth; i++)
	{
		influenceMapDanger[i] = new short[mapHeight];
		influenceMapCover[i] = new short[mapHeight];
		influenceMapCombo[i] = new short[mapHeight];
	}

	// start off unpaused
	paused = false;

	// create the cubemap
	UInt pos;
	Cross::Color* cols;
	GLenum types[6] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	Cross::Bitmap bitmap1("posx.bmp"); Cross::Bitmap bitmap2("negx.bmp"); Cross::Bitmap bitmap3("negy.bmp");
	Cross::Bitmap bitmap4("posy.bmp"); Cross::Bitmap bitmap5("posz.bmp"); Cross::Bitmap bitmap6("negz.bmp");
	Cross::Bitmap* bitmaps[6] = { &bitmap1, &bitmap2, &bitmap3, &bitmap4, &bitmap5, &bitmap6 };
	glGenTextures(1, &cubemap);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	for (UInt i = 0; i < 6; i++)
	{
		UByte* data = new UByte[(*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight() * 3];
		cols = new Cross::Color[(*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight()];
		(*bitmaps[i]).getData(cols);
		pos = 0;
		for (UInt j = 0; j < (*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight(); j++)
		{
			data[pos++] = cols[j].r;
			data[pos++] = cols[j].g;
			data[pos++] = cols[j].b;
		}
		delete[] cols;
		glTexImage2D(types[i], 0, GL_RGB, (*bitmaps[i]).getWidth(), (*bitmaps[i]).getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		delete[] data;
	}

	// create the mesh for displaying influence
	Vector4* positions = new Vector4[mapWidth * mapHeight * 4];
	//Vector3* coords = new Vector3[mapWidth * mapHeight * 4];
	int x = 0, y = 0;
	for (int w = 0; w < mapWidth; w++)
	{
		for (int h = 0; h < mapHeight; h++)
		{
			float infl = influenceMapCombo[w][h] / 255.0f;
			if (infl < -1.0f) infl = -1.0f;
			if (infl > 1.0f) infl = 1.0f;
			positions[x++] = Vector4((float)w, (float)h + 1, 1, infl);
			positions[x++] = Vector4((float)w + 1, (float)h + 1, 2, infl);
			positions[x++] = Vector4((float)w + 1, (float)h, 3, infl);
			positions[x++] = Vector4((float)w, (float)h, 4, infl);
			//coords[y++] = Vector3(0, 0, infl); coords[y++] = Vector3(1, 0, infl); coords[y++] = Vector3(1, 1, infl); coords[y++] = Vector3(0, 1, infl);
		}
	}

	influenceDisplay.addSubMesh(new Cross::SubMesh(mapWidth * mapHeight * 4));
	position = influenceDisplay.getSubMesh(0)->addBuffer(positions);
	//coordinates = influenceDisplay.getSubMesh(0)->addBuffer(coords);
	influenceDisplay.getSubMesh(0)->setQuads(true);

	delete[] positions;
	//delete[] coords;
}

void InfluenceAppCPU::update(const float dt)
{
	// update application window and input
	Application::update(dt);

	// update the camera
	camera->update(dt);

	// update the projection matrix based on the window width and height (aspect ratio)
	projectionMatrix = Matrix::makePerspective(45.0f, window->getWidth() / float(window->getHeight()), 1, 10000);

	// update the entities
	if (!paused)
	{
		for (auto i = entities.begin(); i != entities.end(); i++)
		{
			(*i)->update(dt);
		}
	}

	// allow Q to quit
	if (input->keyPressed(Input::Key::q))
	{
		quit();
	}

	// allow p to pause the game
	if (input->keyPressed(Input::Key::p))
	{
		paused = !paused;
	}

	float startTime = Cross::Time::getCounter();

	// generate the influence maps
	for (int w = 0; w < mapWidth; w++)
	{
		for (int h = 0; h < mapHeight; h++)
		{
			influenceMapDanger[w][h] = 0;
			influenceMapCover[w][h] = 0;
			influenceMapCombo[w][h] = 0;
		}
	}

	// add the negative influences
	for (auto i = enemies.begin(); i != enemies.end(); i++)
	{
		Vector3 position = Vector3((*i)->getPosition().x, (*i)->getPosition().z, 0);
		position += Vector3(100, 100, 0);
		position /= 200.0f;
		position.x *= mapWidth;
		position.y *= mapHeight;
		if ((int)position.x >= 0 && (int)position.y >= 0 && (int)position.x < mapWidth && (int)position.y < mapHeight)
		{
			influenceMapDanger[(int)position.x][(int)position.y] -= influenceAmount;
		}
		Vector3 pos;
		int offset1 = 0;
		for (int j = influenceAmount - falloffAmount; j > 0; j -= falloffAmount)
		{
			offset1++;
			int offset2 = 0;
			for (int k = j - falloffAmount; k > 0; k -= falloffAmount)
			{
				pos = position + Vector3((float)offset1, (float)offset2, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] -= k;
				}
				pos = position + Vector3((float)-offset1, (float)-offset2, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] -= k;
				}
				pos = position + Vector3((float)offset2, (float)-offset1, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] -= k;
				}
				pos = position + Vector3((float)-offset2, (float)offset1, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] -= k;
				}
				offset2++;
			}
		}
	}

	// add the positive influences
	for (auto i = allies.begin(); i != allies.end(); i++)
	{
		Vector3 position = Vector3((*i)->getPosition().x, (*i)->getPosition().z, 0);
		position += Vector3(100, 100, 0);
		position /= 200.0f;
		position.x *= mapWidth;
		position.y *= mapHeight;
		if ((int)position.x >= 0 && (int)position.y >= 0 && (int)position.x < mapWidth && (int)position.y < mapHeight)
		{
			influenceMapDanger[(int)position.x][(int)position.y] += influenceAmount;
		}
		Vector3 pos;
		int offset1 = 0;
		for (int j = influenceAmount - falloffAmount; j > 0; j -= falloffAmount)
		{
			offset1++;
			int offset2 = 0;
			for (int k = j - falloffAmount; k > 0; k -= falloffAmount)
			{
				pos = position + Vector3((float)offset1, (float)offset2, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] += k;
				}
				pos = position + Vector3((float)-offset1, (float)-offset2, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] += k;
				}
				pos = position + Vector3((float)offset2, (float)-offset1, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] += k;
				}
				pos = position + Vector3((float)-offset2, (float)offset1, 0);
				if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
				{
					influenceMapDanger[(int)pos.x][(int)pos.y] += k;
				}
				offset2++;
			}
		}
	}

	// add the chair influences
	for (auto i = covers.begin(); i != covers.end(); i++)
	{
		Vector3 position = Vector3((*i)->getPosition().x, (*i)->getPosition().z, 0);
		position += Vector3(100, 100, 0);
		position /= 200.0f;
		position.x *= mapWidth;
		position.y *= mapHeight;
		if ((int)position.x >= 0 && (int)position.y >= 0 && (int)position.x < mapWidth && (int)position.y < mapHeight)
		{
			influenceMapCover[(int)position.x][(int)position.y] += 50;
		}
		Vector3 pos;
		int offset1 = 0;
		for (int j = 45; j > 0; j -= 10)
		{
			offset1++;
			pos = position + Vector3(0, (float)offset1, 0);
			if ((int)pos.x >= 0 && (int)pos.y >= 0 && (int)pos.x < mapWidth && (int)pos.y < mapHeight)
			{
				influenceMapCover[(int)pos.x][(int)pos.y] += j;
			}
		}
	}

	float endTime = Cross::Time::getCounter();
	float time = endTime - startTime;

	if (input->keyPressed(Cross::Input::Key::o))
	{
		std::cout << time * 1000 << std::endl;
	}

	// combine the influence maps

	startTime = Cross::Time::getCounter();

	for (int w = 0; w < mapWidth; w++)
	{
		for (int h = 0; h < mapHeight; h++)
		{
			influenceMapCombo[w][h] = influenceMapDanger[w][h] + influenceMapCover[w][h];
		}
	}

	endTime = Cross::Time::getCounter();
	time = endTime - startTime;

	if (input->keyPressed(Cross::Input::Key::l))
	{
		std::cout << time * 1000 << std::endl;
	}
}

void InfluenceAppCPU::draw()
{
	// set the viewport to be our entire window
	glViewport(0, 0, window->getWidth(), window->getHeight());

	// start rendering stuffs to the screen and clear it to a new, fresh start!
	graphics->begin();
	graphics->clear(Cross::Color(128, 128, 128));

	// draw the skybox
	shaderSky->begin();
	shaderObjects->setMatrix("projectionMatrix", projectionMatrix);
	shaderObjects->setMatrix("viewMatrix", camera->getView());
	shaderObjects->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(5000, 5000, 5000)) * Matrix::makeTranslation(camera->getPosition()));
	shaderObjects->setTexture("myTexture", cubemap);
	meshSkybox->draw();
	shaderSky->end();

	// initialize the shader for drawing all the objects
	shaderObjects->begin();
	shaderObjects->setMatrix("projectionMatrix", projectionMatrix);
	shaderObjects->setMatrix("viewMatrix", camera->getView());

	// draw the entities
	for (auto i = entities.begin(); i != entities.end(); i++)
	{
		(*i)->draw(shaderObjects);
	}

	// draw the ground
	//shaderObjects->setTexture("texture", textureGround);
	shaderObjects->setTexture("texture", influence->getTexture());
	shaderObjects->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(200, .1f, 200)) * (Matrix::makeRotationY(Math::degToRad(0)) * Matrix::makeRotationX(Math::degToRad(180))) * Matrix::makeTranslation(Vector3(0, 0, 0)));
	meshBox->draw();

	// all done drawing stuffs
	shaderObjects->end();

	try
	{
		//Cross::Mesh influences;
		Vector4* positions = new Vector4[mapWidth * mapHeight * 4];
		//Vector3* coords = new Vector3[mapWidth * mapHeight * 4];
		int x = 0, y = 0;
		for (int w = 0; w < mapWidth; w++)
		{
			for (int h = 0; h < mapHeight; h++)
			{
				float infl = influenceMapCombo[w][h] / 255.0f;
				if (infl < -1.0f) infl = -1.0f;
				if (infl > 1.0f) infl = 1.0f;
				positions[x++] = Vector4((float)w, (float)h + 1, 1, infl);
				positions[x++] = Vector4((float)w + 1, (float)h + 1, 2, infl);
				positions[x++] = Vector4((float)w + 1, (float)h, 3, infl);
				positions[x++] = Vector4((float)w, (float)h, 4, infl);
				//coords[y++] = Vector3(0, 0, infl); coords[y++] = Vector3(1, 0, infl); coords[y++] = Vector3(1, 1, infl); coords[y++] = Vector3(0, 1, infl);
			}
		}

		influenceDisplay.getSubMesh(0)->addBuffer(positions, position);
		//influenceDisplay.getSubMesh(0)->addBuffer(coords, coordinates);
		delete[] positions;
		//delete[] coords;

		influence->begin();
		graphics->clear(Cross::Color(0, 0, 0));
		shaderInfluence->begin();

		shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho((float)mapWidth, 0.0f, 0.0f, (float)mapHeight));
		shaderInfluence->setTexture("texture", textureAlly);
		glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
		influenceDisplay.draw();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		shaderInfluence->end();
		influence->end();
	} catch (...) {}
	
	// present the back buffer and do other cool and exciting stuff
	Application::draw();
	graphics->end();
	graphics->present();
}

void InfluenceAppCPU::uninitialize()
{
	freeptr(camera);
	freeptr(shaderSky);
	freeptr(shaderObjects);
	//freeptr(shaderGround);
	freeptr(shaderScreen);
	freeptr(shaderInfluence);
	freeptr(shaderComboInfluence);
	freeptr(textureAlly);
	freeptr(textureEnemy);
	freeptr(textureGround);
	freeptr(textureFalloff);
	freeptr(textureFalloffCover);
	freeptr(meshSkybox);
	freeptr(meshBox);
	freeptr(meshSphere);
	freeptr(meshSprite);
	freeptr(influence);

	for (int i = 0; i < mapWidth; i++)
	{
		delete[] influenceMapDanger[i];
		delete[] influenceMapCover[i];
		delete[] influenceMapCombo[i];
	}
	delete[] influenceMapDanger;
	delete[] influenceMapCover;
	delete[] influenceMapCombo;

	Application::uninitialize();
}
