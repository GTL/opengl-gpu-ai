#pragma once
#include <Influence Maps/Entities/Entity.h>

class Human : public Entity
{
protected:
	Cross::Mesh* mesh;

	Vector3 position, velocity;

	float influence;
public:
	Human(const Vector3& position, float influence);

	virtual void update(float dt);
	virtual void draw(Cross::Shader* shader) = 0;

	Vector3 getPosition() const;
	float getInfluence() const;
};