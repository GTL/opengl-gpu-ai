#pragma once
#include "Human.h"

class Ally : public Human
{
	Cross::Texture* texture;
public:
	Ally(const Vector3& position, float influence);

	void update(float dt);
	void draw(Cross::Shader* shader);
};