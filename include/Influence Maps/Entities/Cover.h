#pragma once
#include "Entity.h"

class Cover : public Entity
{
	Cross::Texture* texture;

	Cross::Mesh* mesh;

	Vector3 position;

	float influence;
public:
	Cover(const Vector3& position, float influence);

	void update(float dt);
	void draw(Cross::Shader* shader);

	Vector3 getPosition();
	float getInfluence();
};