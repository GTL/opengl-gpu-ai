#pragma once
#include <Cross/Cross.h>

class Entity
{
public:
	virtual void update(float dt) = 0;
	virtual void draw(Cross::Shader* shader) = 0;
};