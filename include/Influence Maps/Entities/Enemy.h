#pragma once
#include "Human.h"

class Enemy : public Human
{
	Cross::Texture* texture;
public:
	Enemy(const Vector3& position, float influence);

	void update(float dt);
	void draw(Cross::Shader* shader);
};