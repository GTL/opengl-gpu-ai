#pragma once
#include <Cross/Cross.h>
#define resMgr ResourceManager::getInstance()

class ResourceManager
{
	std::map<std::string, Cross::Mesh*> meshes;
	std::map<std::string, Cross::Texture*> textures;

	ResourceManager();
public:
	static ResourceManager* getInstance();

	void loadMesh(const std::string& fileName);
	void loadTexture(const std::string& fileName);

	Cross::Mesh* getMesh(const std::string& fileName);
	Cross::Texture* getTexture(const std::string& fileName);
};