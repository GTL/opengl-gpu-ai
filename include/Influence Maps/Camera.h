#pragma once
#include <Cross/Cross.h>

class Camera
{
	Vector3 position;
	float direction, pitch;

	Matrix view;

	Cross::Input* input;

	float speed;
public:
	Camera(Cross::Input* input);

	void update(float dt);

	Vector3 getPosition();

	Vector3 getForward();
	Vector3 getUp();
	Vector3 getRight();

	Matrix getView();

	void setSpeed(float speed);
	float getSpeed();
};