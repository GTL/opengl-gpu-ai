#pragma once
#include <vector>
#include <Cross/Cross.h>
#include <Influence Maps/Camera.h>
#include <Influence Maps/Entities/Entity.h>
#include <Influence Maps/Entities/Enemy.h>
#include <Influence Maps/Entities/Ally.h>
#include <Influence Maps/Entities/Cover.h>

class InfluenceAppCPU : public Cross::Application
{
	Cross::Shader* shaderSky, * shaderObjects, * shaderGround, * shaderScreen;
	Cross::Shader* shaderInfluence, * shaderComboInfluence;
	Cross::Texture* textureAlly, * textureEnemy, * textureGround;
	Cross::Texture* textureFalloff, * textureFalloffCover;
	Cross::Mesh* meshSkybox, * meshBox, * meshSphere, * meshSprite;
	Cross::RenderTarget* influence;

	Matrix projectionMatrix, modelMatrix;

	Camera* camera;

	std::vector<Entity*> entities;
	std::vector<Enemy*> enemies;
	std::vector<Ally*> allies;
	std::vector<Cover*> covers;

	int mapWidth, mapHeight, influenceAmount, falloffAmount;
	bool paused;

	UInt cubemap;

	short** influenceMapDanger, ** influenceMapCover, ** influenceMapCombo;

	Cross::Mesh influenceDisplay;
	UInt position;//, coordinates;
public:
	void initialize();
	void update(const float dt);
	void draw();
	void uninitialize();
};
