#version 130

// uniforms
uniform sampler2D texture;

// varyings
in vec3 coord;

// output
out vec4 out_Color;

void main(void)
{
	// grab the alpha of the falloff texture
	out_Color.a = texture2D(texture, coord.xy).a;

	// get the influence amount (-1 to 1)
	float amt = coord.z;

	// check if that amount is negative...
	if (amt < 0)
	{
		// ... if so, make sure that "amt" has the absolute value
		amt *= -1;
	}

	// do not let the influence exceed 1
	amt = min(amt, 1);

	// check if the original influence is negative...
	if (coord.z < 0)
	{
		// ... if so, make the influence red...
		out_Color.r = out_Color.a * amt;
		out_Color.g = 0;
	}
	else
	{
		// ... otherwise, it's green
		out_Color.g = out_Color.a * amt;
		out_Color.r = 0;
	}

	// no blue here!
	out_Color.b = 0;
}