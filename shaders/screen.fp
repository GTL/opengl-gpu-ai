#version 130

uniform sampler2D myTexture;

in vec2 coord;

out vec4 out_Color;

void main(void)
{
	out_Color = texture2D(myTexture, coord);
}