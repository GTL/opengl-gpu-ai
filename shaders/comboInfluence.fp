#version 130

// uniforms
uniform sampler2D texture1;
uniform sampler2D texture2;

// varyings
in vec2 coord;

// output
out vec4 out_Color;

void main(void)
{
	// fetch the color at this coordinate for each influence maps (only two in this case)
	vec4 col1 = texture2D(texture1, coord);
	vec4 col2 = texture2D(texture2, coord);

	// combine the influences
	float amt = (col1.g + col2.g) - (col1.r + col2.r);

	// set the output color to black with full alpha...
	out_Color = vec4(0, 0, 0, 1);

	// then modify accordingly...
	if (amt < 0)
	{
		// ... if negative, make it red
		out_Color.r = -amt;
	}
	else
	{
		// if positive, make it green
		out_Color.g = amt;
	}
}