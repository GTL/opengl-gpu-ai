#version 130

uniform sampler2D myTexture;

in vec3 coord;

out vec4 out_Color;

void main(void)
{
	out_Color.a = texture2D(myTexture, coord.xy).a;

	float amt = coord.z;
	if (amt < 0)
	{
		amt *= -1;
	}
	amt = min(amt, 1);

	if (coord.z < 0)
	{
		out_Color.r = out_Color.a * amt;
		out_Color.g = 0;
	}
	else
	{
		out_Color.g = out_Color.a * amt;
		out_Color.r = 0;
	}
	out_Color.b = 0;
}