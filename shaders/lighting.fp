#version 130

uniform sampler2D texture;

in vec3 pos;
in vec2 coord;

out vec4 out_Color;

void main(void)
{
	out_Color = texture2D(texture, coord);
}