#version 130

uniform samplerCube texture;

in vec3 pass_Color;
in vec3 pos;
in vec3 coord;

out vec4 out_Color;

void main(void)
{
	//out_Color = vec4(pass_Color, 1.0);
	out_Color = textureCube(texture, normalize(coord));
}