#if defined(_WIN32)
#include <Windows.h>
#endif
#include <Cross/Window.h>
#include <Cross/Exception.h>
#include <Cross/Graphics/Graphics.h>
#include <Cross/Utility/Misc.h>
#include <Influence Maps/InfluenceApp.h>
#include <Influence Maps/InfluenceAppCPU.h>
#include <iostream>
using namespace std;

int main()
{
	try
	{
		char cpu;
		cout << "Run CPU version? y/n: ";
		cin >> cpu;
		if (tolower(cpu) == 'y')
		{
			InfluenceAppCPU app;
			app.run();
		}
		else
		{
			InfluenceApp app;
			app.run();
		}
	}
	catch (Exception& e)
	{
		if (e.result == 0)
		{
			cout << e.file << " (" << e.line << ")" << endl << e.message << endl;
		}
		else
		{
			std::string message = "A called to a windows function failed:\n";
			message += e.message;
			cout << message << endl;
		}
	}
	cout << "fin" << endl;
	return 0;
}

#if defined(_WIN32)
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nCmdShow)
{
	return main();
}
#endif