#include <Influence Maps/ResourceManager.h>

ResourceManager::ResourceManager()
{
}

ResourceManager* ResourceManager::getInstance()
{
	static ResourceManager instance;
	return &instance;
}

void ResourceManager::loadMesh(const std::string& fileName)
{
	meshes.insert(std::make_pair(fileName, Cross::Mesh::loadObj(fileName)));
}

void ResourceManager::loadTexture(const std::string& fileName)
{
	textures.insert(std::make_pair(fileName, new Cross::Texture(fileName)));
}

Cross::Mesh* ResourceManager::getMesh(const std::string& fileName)
{
	std::map<std::string, Cross::Mesh*>::iterator i = meshes.find(fileName);
	if (i == meshes.end())
	{
		loadMesh(fileName);
		i = meshes.find(fileName);
	}
	return i->second;

}

Cross::Texture* ResourceManager::getTexture(const std::string& fileName)
{
	std::map<std::string, Cross::Texture*>::iterator i = textures.find(fileName);
	if (i == textures.end())
	{
		loadTexture(fileName);
		i = textures.find(fileName);
	}
	return i->second;
}