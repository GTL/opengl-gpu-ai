#include <Influence Maps/InfluenceApp.h>
#include <Cross/Utility/Time.h>
#include <Cross/Graphics/Bitmap.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
using namespace std;
using Cross::Input;

void InfluenceApp::initialize()
{
	srand((unsigned int)time(0));

	// initialize the application
	Application::initialize();

	// set the window title
	window->setTitle("Cross Framework - Influence Map");

	// create the camera
	camera = new Camera(input);

	// load and compile the shaders
	shaderSky = new Cross::Shader("shaders/cubemap.vp", "shaders/cubemap.fp");
	shaderObjects = new Cross::Shader("shaders/lighting.vp", "shaders/lighting.fp");
	shaderGround = shaderObjects;
	shaderScreen = new Cross::Shader("shaders/screen.vp", "shaders/screen.fp");
	shaderInfluence = new Cross::Shader("shaders/influence.vp", "shaders/influence.fp");
	shaderComboInfluence = new Cross::Shader("shaders/comboInfluence.vp", "shaders/comboInfluence.fp");

	// initialize the shaders
	shaderSky->bindAttribute(0, "in_Position");
	shaderSky->bindAttribute(1, "in_TexCoords");
	shaderObjects->bindAttribute(0, "in_Position");
	shaderObjects->bindAttribute(1, "in_TexCoords");
	shaderScreen->bindAttribute(0, "in_Position");
	shaderScreen->bindAttribute(1, "in_TexCoords");
	shaderInfluence->bindAttribute(5, "data1");
	shaderInfluence->bindAttribute(1, "data2");
	shaderComboInfluence->bindAttribute(0, "in_Position");
	shaderComboInfluence->bindAttribute(1, "in_TexCoords");

	// load in our textures
	textureAlly = new Cross::Texture("smile.bmp");
	textureEnemy = new Cross::Texture("bolt.bmp");
	textureGround = new Cross::Texture("checkers.bmp");
	textureFalloff = new Cross::Texture("alpha.bmp");
	textureFalloffCover = new Cross::Texture("alpha2.bmp");

	// create the mesh objects
	meshSkybox = Cross::Mesh::loadObj("skybox.obj");
	//meshBox = Cross::Mesh::makeBox();
	meshBox = Cross::Mesh::loadObj("box2.obj");
	meshSphere = Cross::Mesh::loadObj("sphere.obj");
	meshSprite = new Cross::Mesh();
	//Vector2 pos3[] = { Vector2(-1, -1), Vector2(1, -1), Vector2(1, 1), Vector2(-1, 1) };
	//Vector2 tex3[] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	Vector2 pos3[] = { Vector2(-1, 1), Vector2(1, 1), Vector2(1, -1), Vector2(-1, -1) };
	Vector2 tex3[] = { Vector2(0, 1), Vector2(1, 1), Vector2(1, 0), Vector2(0, 0) };
	meshSprite->addSubMesh(new Cross::SubMesh(4));
	meshSprite->getSubMesh(0)->setQuads(true);
	meshSprite->getSubMesh(0)->addBuffer(pos3);
	meshSprite->getSubMesh(0)->addBuffer(tex3);

	// create some enemies and allies for funzies
	for (int i = 0; i < 50; i++)
	{
		switch (rand() % 3)
		{
		case 0:
			entities.push_back(new Enemy(Vector3(float((rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			enemies.push_back((Enemy*)entities[entities.size() - 1]);
			break;
		case 1:
			entities.push_back(new Ally(Vector3((float(rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			allies.push_back((Ally*)entities[entities.size() - 1]);
			break;
		case 2:
			entities.push_back(new Cover(Vector3(float((rand() % 200) - 100), 0.0f, float((rand() % 200) - 100)), .3f));
			covers.push_back((Cover*)entities[entities.size() - 1]);
			break;
		}
	}

	// read in settings from settings.txt
	std::ifstream file;
	file.open("data/settings.txt");
	if (!file.is_open())
	{
		MessageBox(window->getHandle(), "Failed to load data/settings.txt", "Error", MB_OK);
		quit();
		return;
	}
	file >> mapWidth;
	file >> mapHeight;
	
	// create the render targets
	influenceDanger = new Cross::RenderTarget(mapWidth, mapHeight);
	influenceCover = new Cross::RenderTarget(mapWidth, mapHeight);
	influenceCombo = new Cross::RenderTarget(mapWidth, mapHeight);

	// start off unpaused
	paused = false;

	// create the cubemap
	UInt pos;
	Cross::Color* cols;
	GLenum types[6] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	Cross::Bitmap bitmap1("posx.bmp"); Cross::Bitmap bitmap2("negx.bmp"); Cross::Bitmap bitmap3("negy.bmp");
	Cross::Bitmap bitmap4("posy.bmp"); Cross::Bitmap bitmap5("posz.bmp"); Cross::Bitmap bitmap6("negz.bmp");
	Cross::Bitmap* bitmaps[6] = { &bitmap1, &bitmap2, &bitmap3, &bitmap4, &bitmap5, &bitmap6 };
	glGenTextures(1, &cubemap);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	for (UInt i = 0; i < 6; i++)
	{
		UByte* data = new UByte[(*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight() * 3];
		cols = new Cross::Color[(*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight()];
		(*bitmaps[i]).getData(cols);
		pos = 0;
		for (UInt j = 0; j < (*bitmaps[i]).getWidth() * (*bitmaps[i]).getHeight(); j++)
		{
			data[pos++] = cols[j].r;
			data[pos++] = cols[j].g;
			data[pos++] = cols[j].b;
		}
		delete[] cols;
		glTexImage2D(types[i], 0, GL_RGB, (*bitmaps[i]).getWidth(), (*bitmaps[i]).getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		delete[] data;
	}

	// create influence meshes
	{ // danger mesh
		int bufferSize = enemies.size() + allies.size();
		Vector4* positions = new Vector4[bufferSize * 4];
		int x = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = enemies.begin(); i != enemies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, -(*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		for (auto i = allies.begin(); i != allies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		meshInfluence1.addSubMesh(new Cross::SubMesh(bufferSize * 4));
		position1 = meshInfluence1.getSubMesh(0)->addBuffer(positions);
		meshInfluence1.getSubMesh(0)->setQuads(true);
		delete[] positions;
	}
	{ // cover mesh
		int bufferSize = covers.size();
		Vector4* positions = new Vector4[bufferSize * 4];
		int x = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = covers.begin(); i != covers.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		for (; x < bufferSize * 4; x++)
		{
			positions[x] = Vector4(0, 0, 1, 0);
		}
		meshInfluence2.addSubMesh(new Cross::SubMesh(bufferSize * 4));
		position2 = meshInfluence2.getSubMesh(0)->addBuffer(positions);
		//meshInfluence1.getSubMesh(0)->addTextureCoordinates(coords);
		meshInfluence2.getSubMesh(0)->setQuads(true);
		delete[] positions;
	}
	/*{ // cover mesh
		Vector4* positions = new Vector4[covers.size() * 4];
		int x = 0, y = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = covers.begin(); i != covers.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			//coords[y++] = Vector3(0, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 1, (*i)->getInfluence()); coords[y++] = Vector3(0, 1, (*i)->getInfluence());
		}

		meshInfluence2.addSubMesh(new Cross::SubMesh(covers.size() * 4));
		position2 = meshInfluence2.getSubMesh(0)->addPositions(positions);
		//meshInfluence2.getSubMesh(0)->addTextureCoordinates(coords);
		meshInfluence2.getSubMesh(0)->setQuads(true);

		delete[] positions;
	}*/
}

void InfluenceApp::update(const float dt)
{
	// update application window and input
	Application::update(dt);

	// update the camera
	camera->update(dt);

	// update the projection matrix based on the window width and height (aspect ratio)
	projectionMatrix = Matrix::makePerspective(45.0f, window->getWidth() / float(window->getHeight()), 1, 10000);

	// update the entities
	if (!paused)
	{
		for (auto i = entities.begin(); i != entities.end(); i++)
		{
			(*i)->update(dt);
		}
	}

	// allow Q to quit
	if (input->keyPressed(Input::Key::q))
	{
		quit();
	}

	// allow p to pause the game
	if (input->keyPressed(Input::Key::p))
	{
		paused = !paused;
	}

	// clickity click
	if (input->mousePressed(0))
	{
		Vector2 mousePos = input->getMousePosition();
		if (mousePos.x > window->getWidth() - 128 && mousePos.y < 128 && mousePos.x < window->getWidth() && mousePos.y >= 0)
		{
			influenceDanger->begin();
			Cross::Color color;
			Vector2 pos;
			pos.x = (mousePos.x - (window->getWidth() - 128)) * (mapWidth / 128.0f);
			pos.y = mapHeight - ((mousePos.y) * (mapHeight / 128.0f));
			glReadPixels((GLsizei)pos.x, (GLsizei)pos.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &color.r);
			std::stringstream ss(std::stringstream::in | std::stringstream::out);
			ss << ((int)color.g - (int)color.r);
			influenceDanger->end();
			MessageBox(window->getHandle(), ss.str().c_str(), "Influence Amount", MB_OK);
		}
		if (mousePos.x > window->getWidth() - 128 && mousePos.y < 256 && mousePos.x < window->getWidth() && mousePos.y >= 128)
		{
			influenceCover->begin();
			Cross::Color color;
			Vector2 pos;
			pos.x = (mousePos.x - (window->getWidth() - 128)) * (mapWidth / 128.0f);
			pos.y = mapHeight - ((mousePos.y - 128) * (mapHeight / 128.0f));
			glReadPixels((GLsizei)pos.x, (GLsizei)pos.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &color.r);
			std::stringstream ss(std::stringstream::in | std::stringstream::out);
			ss << ((int)color.g - (int)color.r);
			influenceCover->end();
			MessageBox(window->getHandle(), ss.str().c_str(), "Influence Amount", MB_OK);
		}
		if (mousePos.x > window->getWidth() - 128 && mousePos.y < 384 && mousePos.x < window->getWidth() && mousePos.y >= 256)
		{
			influenceCombo->begin();
			Cross::Color color;
			Vector2 pos;
			pos.x = (mousePos.x - (window->getWidth() - 128)) * (mapWidth / 128.0f);
			pos.y = mapHeight - ((mousePos.y - 256) * (mapHeight / 128.0f));
			glReadPixels((GLsizei)pos.x, (GLsizei)pos.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &color.r);
			std::stringstream ss(std::stringstream::in | std::stringstream::out);
			ss << ((int)color.g - (int)color.r);
			influenceCombo->end();
			MessageBox(window->getHandle(), ss.str().c_str(), "Influence Amount", MB_OK);
		}
	}
}

void InfluenceApp::draw()
{
	// set the viewport to be our entire window
	glViewport(0, 0, window->getWidth(), window->getHeight());

	// start rendering stuffs to the screen and clear it to a new, fresh start!
	graphics->begin();
	graphics->clear(Cross::Color(128, 128, 128));

	// draw the skybox
	shaderSky->begin();
	shaderObjects->setMatrix("projectionMatrix", projectionMatrix);
	shaderObjects->setMatrix("viewMatrix", camera->getView());
	shaderObjects->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(5000, 5000, 5000)) * Matrix::makeTranslation(camera->getPosition()));
	shaderObjects->setTexture("myTexture", cubemap);
	meshSkybox->draw();
	shaderSky->end();

	// initialize the shader for drawing all the objects
	shaderObjects->begin();
	shaderObjects->setMatrix("projectionMatrix", projectionMatrix);
	shaderObjects->setMatrix("viewMatrix", camera->getView());

	// draw the entities
	for (auto i = entities.begin(); i != entities.end(); i++)
	{
		(*i)->draw(shaderObjects);
	}

	// draw the ground
	shaderObjects->setTexture("texture", textureGround);
	shaderObjects->setTexture("texture", influenceCombo->getTexture());
	shaderObjects->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(200, .1f, 200)) * (Matrix::makeRotationY(Math::degToRad(0)) * Matrix::makeRotationX(Math::degToRad(180))) * Matrix::makeTranslation(Vector3(0, 0, 0)));
	meshBox->draw();

	// all done drawing stuffs
	shaderObjects->end();

	float startTime;

	// update the influence meshes
	{ // danger mesh
		int bufferSize = enemies.size() + allies.size();
		Vector4* positions = new Vector4[bufferSize * 4];
		int x = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = enemies.begin(); i != enemies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, -(*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		for (auto i = allies.begin(); i != allies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		meshInfluence1.getSubMesh(0)->addBuffer(positions, position1);
		delete[] positions;
	}
	{ // cover mesh
		int bufferSize = covers.size();
		Vector4* positions = new Vector4[bufferSize * 4];
		int x = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = covers.begin(); i != covers.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		for (; x < bufferSize * 4; x++)
		{
			positions[x] = Vector4(0, 0, 1, 0);
		}
		meshInfluence2.getSubMesh(0)->addBuffer(positions, position2);
		delete[] positions;
	}
	influenceDanger->begin();
	graphics->clear(Cross::Color(0, 0, 0));
	shaderInfluence->begin();
	shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho((float)mapWidth, 0.0f, 0.0f, (float)mapHeight));
	shaderInfluence->setTexture("texture", textureFalloff);
	glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
	meshInfluence1.draw();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	shaderInfluence->end();
	influenceDanger->end();

	influenceCover->begin();
	graphics->clear(Cross::Color(0, 0, 0));
	shaderInfluence->begin();
	shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho((float)mapWidth, 0.0f, 0.0f, (float)mapHeight));
	shaderInfluence->setTexture("texture", textureFalloffCover);
	glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
	meshInfluence2.draw();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	influenceCover->end();
	influenceDanger->end();

	/*{
		startTime = Cross::Time::getCounter();

		int bufferSize = max(enemies.size() + allies.size(), covers.size());
		Vector4* positions1 = new Vector4[bufferSize * 4];
		Vector4* positions2 = new Vector4[bufferSize * 4];
		int x = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = enemies.begin(); i != enemies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, -(*i)->getInfluence());
			positions1[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
		}
		for (auto i = allies.begin(); i != allies.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions1[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions1[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			//coords[y++] = Vector3(0, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 1, (*i)->getInfluence()); coords[y++] = Vector3(0, 1, (*i)->getInfluence());
		}
		for (; x < bufferSize * 4; x++)
		{
			positions1[x] = Vector4(0, 0, 1, 0);
		}
		x = 0;
		for (auto i = covers.begin(); i != covers.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions2[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions2[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions2[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions2[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			//coords[y++] = Vector3(0, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 1, (*i)->getInfluence()); coords[y++] = Vector3(0, 1, (*i)->getInfluence());
		}
		for (; x < bufferSize * 4; x++)
		{
			positions2[x] = Vector4(0, 0, 1, 0);
		}

		meshInfluence1.getSubMesh(0)->addBuffer(positions1, position1);
		meshInfluence1.getSubMesh(0)->addBuffer(positions2, position2);

		influenceDanger->begin();
		graphics->clear(Cross::Color(0, 0, 0));

		shaderInfluence->begin();
		shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho((float)mapWidth, 0.0f, 0.0f, (float)mapHeight));
		shaderInfluence->setTexture("texture", textureFalloff);
		shaderInfluence->setTexture("texture2", textureFalloffCover);
		glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
		meshInfluence1.draw();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		shaderInfluence->end();
		influenceDanger->end();

		delete[] positions1;
		delete[] positions2;
	}*/

	/*{
		//Cross::Mesh influences;
		Vector4* positions = new Vector4[covers.size() * 4];
		//Vector3* coords = new Vector3[covers.size() * 4];
		int x = 0, y = 0;
		float size = mapWidth * 0.3125f;
		for (auto i = covers.begin(); i != covers.end(); i++)
		{
			Vector4 position((*i)->getPosition().x * (mapWidth / 200.0f), (*i)->getPosition().z * (mapHeight / 200.0f), 0, (*i)->getInfluence());
			positions[x++] = position + Vector4(-size, size, 1, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, size, 2, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(size, -size, 3, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			positions[x++] = position + Vector4(-size, -size, 4, 0) + Vector4(float(mapWidth / 2), float(mapHeight / 2), 0, 0);
			//coords[y++] = Vector3(0, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 0, (*i)->getInfluence()); coords[y++] = Vector3(1, 1, (*i)->getInfluence()); coords[y++] = Vector3(0, 1, (*i)->getInfluence());
		}

		//influences.addSubMesh(new Cross::SubMesh(covers.size() * 4));
		meshInfluence2.getSubMesh(0)->addPositions(positions, position2);
		//influences.getSubMesh(0)->addTextureCoordinates(coords);
		//influences.getSubMesh(0)->setQuads(true);

		influenceCover->begin();
		graphics->clear(Cross::Color(0, 0, 0));
		shaderInfluence->begin();

		shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho((float)mapWidth, 0.0f, 0.0f, (float)mapHeight));
		shaderInfluence->setTexture("texture1", textureFalloffCover);
		glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
		meshInfluence2.draw();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		shaderInfluence->end();
		influenceCover->end();

		delete[] positions;
		//delete[] coords;
	}*/

	float endTime = Cross::Time::getCounter();
	startTime = 0;
	float time = endTime - startTime;

	// press O to see the time it took to create the influence maps
	if (input->keyPressed(Cross::Input::Key::o))
	{
		std::cout << time * 1000 << std::endl;
	}

	// reset the timer
	startTime = Cross::Time::getCounter();

	// create the combo influence map
	influenceCombo->begin();
	shaderComboInfluence->begin();
	shaderComboInfluence->setTexture("texture1", influenceDanger->getTexture());
	shaderComboInfluence->setTexture("texture2", influenceCover->getTexture());
	meshSprite->draw();
	shaderComboInfluence->end();
	influenceCombo->end();

	endTime = Cross::Time::getCounter();
	time = endTime - startTime;

	// press l to see how long it took to create the combo influence map
	if (input->keyPressed(Cross::Input::Key::l))
	{
		std::cout << time * 1000 << std::endl;
	}

	// draw the influence maps
	shaderScreen->begin();
	glViewport(0, 0, (GLsizei)window->getWidth(), (GLsizei)window->getHeight());
	shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho((float)window->getWidth(), 0.0f, 0.0f, (float)window->getHeight()));
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(64.0f, 64.0f)) * Matrix::makeTranslation(Vector2(window->getWidth() - 64.0f, window->getHeight() - 64.0f)));
	shaderScreen->setTexture("myTexture", influenceDanger->getTexture());
	meshSprite->draw();
	shaderScreen->end();
	shaderScreen->begin();
	shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho((float)window->getWidth(), 0.0f, 0.0f, (float)window->getHeight()));
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(64.0f, 64.0f)) * Matrix::makeTranslation(Vector2(float(window->getWidth() - 64), float(window->getHeight() - 64 - 128))));
	shaderScreen->setTexture("myTexture", influenceCover->getTexture());
	meshSprite->draw();
	shaderScreen->end();
	shaderScreen->begin();
	shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho((float)window->getWidth(), 0.0f, 0.0f, (float)window->getHeight()));
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(64.0f, 64.0f)) * Matrix::makeTranslation(Vector2(float(window->getWidth() - 64), float(window->getHeight() - 64 - 256))));
	shaderScreen->setTexture("myTexture", influenceCombo->getTexture());
	meshSprite->draw();
	shaderScreen->end();
	
	// present the back buffer and do other cool and exciting stuff
	Application::draw();
	graphics->end();
	graphics->present();
}

void InfluenceApp::uninitialize()
{
	freeptr(camera);
	freeptr(shaderSky);
	freeptr(shaderObjects);
	//freeptr(shaderGround);
	freeptr(shaderScreen);
	freeptr(shaderInfluence);
	freeptr(shaderComboInfluence);
	freeptr(textureAlly);
	freeptr(textureEnemy);
	freeptr(textureGround);
	freeptr(textureFalloff);
	freeptr(textureFalloffCover);
	freeptr(meshSkybox);
	freeptr(meshBox);
	freeptr(meshSphere);
	freeptr(meshSprite);
	freeptr(influenceDanger);
	freeptr(influenceCover);
	freeptr(influenceCombo);

	Application::uninitialize();
}
