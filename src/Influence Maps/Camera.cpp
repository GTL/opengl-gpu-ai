#include <Influence Maps/Camera.h>
using Cross::Input;

Camera::Camera(Cross::Input* input)
{
	position = Vector3(0, 0, -15);
	direction = 90.0f;
	pitch = 0.0f;

	this->input = input;
}

void Camera::update(float dt)
{
	if (input->mouseDown(0))
	{
		direction -= input->getMouseMove().x;// / 100.0f;
		pitch -= input->getMouseMove().y;// / 100.0f;
	}

	Vector3 lookTo = Vector3(Math::cos(Math::degToRad(direction)), Math::tan(Math::degToRad(pitch)), Math::sin(Math::degToRad(direction)));
	view = Matrix::makeLookAt(position, position + lookTo, Vector3(0, 1, 0));

	if (input->keyDown(Input::Key::w))
	{
		position += getForward() * 50 * dt;
	}
	if (input->keyDown(Input::Key::s))
	{
		position -= getForward() * 50 * dt;
	}
	if (input->keyDown(Input::Key::a))
	{
		position -= getRight() * 50 * dt;
	}
	if (input->keyDown(Input::Key::d))
	{
		position += getRight() * 50 * dt;
	}
	if (input->keyDown(Input::Key::space))
	{
		position += getUp() * 50 * dt;
	}
	if (input->keyDown(Input::Key::leftShift))
	{
		position -= getUp() * 50 * dt;
	}
}

/*void Camera::update(float dt)
{
	float offset = 50;
	position = Vector3(Math::sin(Math::degToRad(input->getMousePosition().x)), Math::tan(Math::degToRad(input->getMousePosition().y)), Math::cos(Math::degToRad(input->getMousePosition().x)));
	position.normalize();
	position *= offset;
	view = Matrix::makeLookAt(position, Vector3(0, 0, 0), Vector3(0, 1, 0));
}*/

Vector3 Camera::getPosition()
{
	return position;
}

Vector3 Camera::getForward()
{
	return Vector3(view[0][2], view[1][2], view[2][2]);
}

Vector3 Camera::getUp()
{
	return Vector3(view[0][1], view[1][1], view[2][1]);
}

Vector3 Camera::getRight()
{
	return Vector3(view[0][0], view[1][0], view[2][0]);
}

Matrix Camera::getView()
{
	return view;
}

void Camera::setSpeed(float speed)
{
	this->speed = speed;
}

float Camera::getSpeed()
{
	return speed;
}