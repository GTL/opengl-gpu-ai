#include <Influence Maps/Entities/Cover.h>
#include <Influence Maps/ResourceManager.h>

Cover::Cover(const Vector3& position, float influence)
{
	texture = resMgr->getTexture("wood.bmp");
	mesh = resMgr->getMesh("chair.obj");

	this->position = position;
	this->influence = influence;
}

void Cover::update(float dt)
{
}

void Cover::draw(Cross::Shader* shader)
{
	shader->setTexture("texture", texture);
	shader->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(10, 10, 10)) * Matrix::makeRotationY(Math::pi) * Matrix::makeTranslation(position + Vector3(0, 5, -2)));
	mesh->draw();
}

Vector3 Cover::getPosition()
{
	return position;
}

float Cover::getInfluence()
{
	return influence;
}