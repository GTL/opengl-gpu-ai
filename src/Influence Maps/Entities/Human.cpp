#pragma once
#include <Influence Maps/Entities/Human.h>
#include <Influence Maps/ResourceManager.h>

Human::Human(const Vector3& position, float influence)
{
	mesh = resMgr->getMesh("sphere.obj");
	this->position = position;
	this->influence = influence;
	velocity = Vector3(0, 0, 0);
}

void Human::update(float dt)
{
	position += velocity * dt;
}

Vector3 Human::getPosition() const
{
	return position;
}

float Human::getInfluence() const
{
	return influence;
}