#include <Influence Maps/Entities/Enemy.h>
#include <Influence Maps/ResourceManager.h>

Enemy::Enemy(const Vector3& position, float influence) : Human(position, influence)
{
	texture = resMgr->getTexture("bolt.bmp");
}

void Enemy::update(float dt)
{
	// generate a random point ahead
	Vector3 pointAhead = position + (Vector3::normalized(velocity) * 50.0f);
	float circleRadius = 30;
	float angle = float(rand() % 360);
	Vector3 pos = pointAhead + Vector3(Math::cos(Math::degToRad(angle)) * circleRadius, 0, Math::sin(Math::degToRad(angle)) * circleRadius);

	// seek the point
	Vector3 dir = pos - position;
	dir.normalize();
	dir *= 10;
	velocity += dir;
	if (velocity.getLength() > 20)
	{
		velocity.normalize();
		velocity *= 20;
	}

	// update the base class stuffs
	Human::update(dt);

	// wrap
	if (position.x < -100) position.x = 100;
	if (position.z < -100) position.z = 100;
	if (position.x > 100) position.x = -100;
	if (position.z > 100) position.z = -100;
}

void Enemy::draw(Cross::Shader* shader)
{
	Vector3 dir = Vector3::normalized(velocity);
	shader->setTexture("texture", texture);
	shader->setMatrix("modelMatrix", Matrix::makeScaling(Vector3(5, 5, 5)) * Matrix::makeRotationY(Math::atan2(dir.x, dir.z)) * Matrix::makeTranslation(position + Vector3(0, 2.5f, 0)));
	mesh->draw();
}