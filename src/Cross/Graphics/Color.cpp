#include <Cross/Graphics/Color.h>

Cross::Color Cross::Color::makeHSV(const int hue, const int saturation, const int value)
{
	Color ret;

	int f, p, q, t, h;
 
	if (saturation == 0)
	{
		ret.r = ret.g = ret.b = value;
		return ret;
	}
 
	f = ((hue % 60) * 255) / 60;
	h = (hue % 360) / 60;
 
	p = (value * (256 - saturation)) / 256;
	q = (value * (256 - (saturation * f) / 256 )) / 256;
	t = (value * (256 - (saturation * (256 - f)) / 256)) / 256;
 
	switch (h)
	{
	case 0:
		ret.r = value;
		ret.g = t;
		ret.b = p;
		break;
	case 1:
		ret.r = q;
		ret.g = value;
		ret.b = p;
		break;
	case 2:
		ret.r = p;
		ret.g = value;
		ret.b = t;
		break;
	case 3:
		ret.r = p;
		ret.g = q;
		ret.b = value;
		break;
	case 4:
		ret.r = t;
		ret.g = p;
		ret.b = value;
		break;
	default:
		ret.r = value;
		ret.g = p;
		ret.b = q;
		break;
	}
	return ret;
}

Cross::Color::Color()
{
	r = g = b = 0;
	a = 255;
}

Cross::Color::Color(UByte r, UByte g, UByte b)
{
	this->r = r;
	this->g = g;
	this->b = b;
	a = 255;
}

Cross::Color::Color(UByte r, UByte g, UByte b, UByte a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}
