#include <Cross/Graphics/RenderTarget.h>

Cross::RenderTarget::Viewport Cross::RenderTarget::viewport;

Cross::RenderTarget::RenderTarget(UInt width, UInt height, UInt attachment)
{
	this->width = width;
	this->height = height;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + attachment, GL_TEXTURE_2D, texture, 0);
	
	/*glGenRenderbuffers(1, &renderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER_EXT, renderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, renderBuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);*/
}

Cross::RenderTarget::~RenderTarget()
{
	glDeleteTextures(1, &texture);
	glDeleteFramebuffers(1, &frameBuffer);
}

UInt Cross::RenderTarget::getTexture()
{
	return texture;
}

void Cross::RenderTarget::begin()
{
	glGetFloatv(GL_VIEWPORT, viewport.vp);
	glViewport(0, 0, width, height);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	//glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
}

void Cross::RenderTarget::end()
{
	glViewport(viewport.x, viewport.y, viewport.width, viewport.height);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}