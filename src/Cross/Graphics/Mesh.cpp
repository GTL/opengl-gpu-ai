#include <vector>
#include <map>
#include <Cross/Graphics/Mesh.h>
#include <Cross/Utility/File.h>
#include <Cross/Utility/Misc.h>
#include <Cross/Exception.h>

Cross::Mesh* Cross::Mesh::makeBox()
{
	// create our vertex arrays for creating the box and skybox
	Vector3 vertices[36];
	Cross::Color colors[36];
	Vector2 coords[36];

	// create box
	{
		// back
		vertices[ 0] = Vector3(-0.5, -0.5, 0.5); vertices[ 1] = Vector3(-0.5, 0.5, 0.5); vertices[ 2] = Vector3(0.5, 0.5, 0.5);
		vertices[ 3] = Vector3(0.5, -0.5, 0.5); vertices[ 4] = Vector3(-0.5, -0.5, 0.5); vertices[ 5] = Vector3(0.5, 0.5, 0.5);
		coords[ 0] = Vector2(1, 0); coords[ 1] = Vector2(1, 1); coords[ 2] = Vector2(0, 1);
		coords[ 3] = Vector2(0, 0); coords[ 4] = Vector2(1, 0); coords[ 5] = Vector2(0, 1);

		// front
		vertices[ 6] = Vector3(0.5, 0.5, -0.5); vertices[ 7] = Vector3(-0.5, 0.5, -0.5); vertices[ 8] = Vector3(-0.5, -0.5, -0.5);
		vertices[ 9] = Vector3(0.5, 0.5, -0.5); vertices[10] = Vector3(-0.5, -0.5, -0.5); vertices[11] = Vector3(0.5, -0.5, -0.5);
		coords[ 9] = Vector2(1, 1); coords[10] = Vector2(0, 0); coords[11] = Vector2(1, 0);
		coords[ 6] = Vector2(1, 1); coords[ 7] = Vector2(0, 1); coords[ 8] = Vector2(0, 0);

		// right
		vertices[12] = Vector3(0.5, 0.5, 0.5); vertices[13] = Vector3(0.5, 0.5, -0.5); vertices[14] = Vector3(0.5, -0.5, -0.5);
		vertices[15] = Vector3(0.5, 0.5, 0.5); vertices[16] = Vector3(0.5, -0.5, -0.5); vertices[17] = Vector3(0.5, -0.5, 0.5);
		coords[12] = Vector2(1, 1); coords[13] = Vector2(0, 1); coords[14] = Vector2(0, 0);
		coords[15] = Vector2(1, 1); coords[16] = Vector2(0, 0); coords[17] = Vector2(1, 0);

		// left
		vertices[18] = Vector3(-0.5, -0.5, -0.5); vertices[19] = Vector3(-0.5, 0.5, -0.5); vertices[20] = Vector3(-0.5, 0.5, 0.5);
		vertices[21] = Vector3(-0.5, -0.5, 0.5); vertices[22] = Vector3(-0.5, -0.5, -0.5); vertices[23] = Vector3(-0.5, 0.5, 0.5);
		coords[18] = Vector2(1, 0); coords[19] = Vector2(1, 1); coords[20] = Vector2(0, 1);
		coords[21] = Vector2(0, 0); coords[22] = Vector2(1, 0); coords[23] = Vector2(0, 1);

		// bottom
		vertices[24] = Vector3(0.5, -0.5, 0.5); vertices[25] = Vector3(0.5, -0.5, -0.5); vertices[26] = Vector3(-0.5, -0.5, -0.5);
		vertices[27] = Vector3(0.5, -0.5, 0.5); vertices[28] = Vector3(-0.5, -0.5, -0.5); vertices[29] = Vector3(-0.5, -0.5, 0.5);
		coords[24] = Vector2(1, 0); coords[25] = Vector2(1, 1); coords[26] = Vector2(0, 1);
		coords[27] = Vector2(1, 0); coords[28] = Vector2(0, 1); coords[29] = Vector2(0, 0);

		// top
		vertices[30] = Vector3(-0.5, 0.5, -0.5); vertices[31] = Vector3(0.5, 0.5, -0.5); vertices[32] = Vector3(0.5, 0.5, 0.5);
		vertices[33] = Vector3(-0.5, 0.5, 0.5); vertices[34] = Vector3(-0.5, 0.5, -0.5); vertices[35] = Vector3(0.5, 0.5, 0.5);
		coords[30] = Vector2(0, 0); coords[31] = Vector2(1, 0); coords[32] = Vector2(1, 1);
		coords[33] = Vector2(0, 1); coords[34] = Vector2(0, 0); coords[35] = Vector2(1, 1);

		// generate colors
		for (int i = 0; i < 36; i++)
		{
			colors[i].r = UByte(255 * (i / 36.0f));
			colors[i].g = 0;
			colors[i].b = 0;
			colors[i].a = 255;
		}
	}

	// create the mesh object
	SubMesh* box = new SubMesh(36);
	box->addBuffer(vertices);
	box->addBuffer(coords);
	box->addBuffer(colors);

	Mesh* mesh = new Mesh();
	mesh->addSubMesh(box);

	return mesh;
}

Cross::Mesh* Cross::Mesh::makeSkyBox()
{
	return nullptr;
}

Cross::Mesh* Cross::Mesh::loadObj(const std::string& fileName)
{
	struct Index
	{
		std::vector<int> vertices, textureCoords, normals;
	};

	std::vector<std::pair<Vector3, int>> vertices;
	std::vector<std::pair<Vector3, int>> textureCoords;
	std::vector<std::pair<Vector3, int>> normals;
	std::map<std::string, Index> indices;

	std::string result;
	Cross::File::getText(fileName, result);

	std::string groupName = "default";

	for (UInt i = 0; i < result.size();)
	{
		char cur = result[i++];
		if (cur == ' ' || cur == 13 || cur == 10) // skip excessive spacing
		{
			continue;
		}
		else
		{
			bool skipLine = false;
			switch (tolower(cur))
			{
			case '#': // comments
				{
					skipLine = true;
				}
				break;
			case 'v':
				{
					std::vector<std::pair<Vector3, int>>* pull;
					if (cur == 'v' && result[i] == ' ')
					{
						while (result[++i] == ' ');
						pull = &vertices;
					}
					else if (result[i] == 't' && result[i + 1] == ' ')
					{
						while (result[++i] == ' ');
						pull = &textureCoords;
					}
					else if (result[i] == 'n' && result[i + 1] == ' ')
					{
						while (result[++i] == ' ');
						pull = &normals;
					}
					else
					{
						Ex("Invalid obj file.");
					}
					std::vector<std::string> data;
					do
					{
						std::string number;
						while (isdigit(result[i]) || result[i] == '-' || result[i] == '.')
						{
							number += result[i];
							i++;
						}
						data.push_back(number);
						if (result[i] != ' ' && result[i] != 13 && result[i] != 10)
						{
							char b = result[i];
							skipLine = true;
							break;
						}
						if (result[i] == ' ')
						{
							while (result[++i] == ' ');
						}
					}
					while (result[i] != 13 && result[i] != 10);
					if (!skipLine)
					{
						skipLine = (data.size() == 0);
					}
					if (!skipLine)
					{
						if (data.size() < 2)
						{
							Ex("Cannot load mesh with less than 2D components.");
						}
						else if (data.size() > 3)
						{
							Ex("Cannot load mesh with more than 3D components.");
						}
						else
						{
							Vector3 insert;
							for (unsigned int j = 0; j < data.size(); j++)
							{
								insert[j] = stringTo<float>(data[j]);
							}
							pull->push_back(std::make_pair(insert, data.size()));
						}
					}
				}
				break;
			case 'f':
				{
					Index* pull;
					if (cur == 'f' && result[i] == ' ')
					{
						while (result[++i] == ' ');
						auto i = indices.find(groupName);
						if (i == indices.end())
						{
							indices.insert(std::make_pair(groupName, Index()));
							i = indices.find(groupName);
						}
						pull = &i->second;
					}
					else
					{
						Ex("Invalid obj file.");
					}
					std::vector<std::string> data;
					do
					{
						std::string number;
						while (isdigit(result[i]) || result[i] == '-' || result[i] == '.' || result[i] == '/')
						{
							number += result[i];
							i++;
						}
						data.push_back(number);
						if (result[i] != ' ' && result[i] != 13 && result[i] != 10)
						{
							char b = result[i];
							skipLine = true;
							break;
						}
						if (result[i] == ' ')
						{
							while (result[++i] == ' ');
						}
					}
					while (result[i] != 13 && result[i] != 10);
					if (!skipLine)
					{
						skipLine = (data.size() == 0);
					}
					if (!skipLine)
					{
						if (data.size() < 2)
						{
							Ex("Cannot load mesh with less than 2D components.");
						}
						else if (data.size() > 3)
						{
							Ex("Cannot load mesh with more than 3D components.");
						}
						else
						{
							for (unsigned int j = 0; j < data.size(); j++)
							{
								if (data[j][0] == '/' || data[j][data[j].length() - 1] == '/')
								{
									Ex("Invalid obj file.");
								}
								if (data[j].find('/') == std::string::npos)
								{
									pull->vertices.push_back(stringTo<int>(data[j]));
								}
								else
								{
									data[j] += "/";
									int count = 0;
									while (data[j].find('/') != std::string::npos)
									{
										count++;
										std::string element = data[j].substr(0, data[j].find('/'));
										if (element == "")
										{
											data[j] = data[j].substr(data[j].find('/') + 1);
											continue;
										}
										switch (count)
										{
										case 1:
											pull->vertices.push_back(stringTo<int>(element));
											break;
										case 2:
											pull->textureCoords.push_back(stringTo<int>(element));
											break;
										case 3:
											pull->normals.push_back(stringTo<int>(element));
											break;
										case 4:
											Ex("Invalid obj file.");
											break;
										}
										data[j] = data[j].substr(data[j].find('/') + 1);
									}
								}
							}
						}
					}
				}
				break;
			case 'g':
				{
					if (result[i++] != ' ')
					{
						Ex("Invalid obj file.");
					}
					groupName = "";
					while (result[i] != 13 && result[i] != 10)
					{
						groupName += result[i++];
					}
				}
				break;
			/*default:
				Ex("Invalid obj file.");
				break;*/
			}
			if (skipLine)
			{
				do
				{
					cur = result[i++];
				} while (cur != 13 && cur != 10);
			}
		}
	}

	Mesh* newMesh = new Mesh();

	for (auto i = indices.begin(); i != indices.end(); i++)
	{
		if (i->second.vertices.size() == 0)
		{
			Ex("Invalid obj file.");
		}
		if (i->second.textureCoords.size() != 0 && i->second.vertices.size() != i->second.textureCoords.size())
		{
			Ex("Invalid obj file.");
		}
		if (i->second.normals.size() != 0 && i->second.vertices.size() != i->second.normals.size())
		{
			Ex("Invalid obj file.");
		}
		SubMesh* newSubMesh = new SubMesh(i->second.vertices.size());
		if (vertices[i->second.vertices[0]].second == 2)
		{
			Vector2* positions = new Vector2[i->second.vertices.size()];
			for (UInt j = 0; j < i->second.vertices.size(); j++)
			{
				positions[j] = vertices[i->second.vertices[j] - 1].first;
			}
			newSubMesh->addBuffer(positions);
			delete[] positions;
		}
		else
		{
			Vector3* positions = new Vector3[i->second.vertices.size()];
			for (UInt j = 0; j < i->second.vertices.size(); j++)
			{
				positions[j] = vertices[i->second.vertices[j] - 1].first;
			}
			newSubMesh->addBuffer(positions);
			delete[] positions;
		}
		if (i->second.textureCoords.size() != 0)
		{
			if (textureCoords[i->second.textureCoords[0]].second == 2)
			{
				Vector2* texs = new Vector2[i->second.textureCoords.size()];
				for (UInt j = 0; j < i->second.textureCoords.size(); j++)
				{
					texs[j] = textureCoords[i->second.textureCoords[j] - 1].first;
				}
				newSubMesh->addBuffer(texs);
				delete[] texs;
			}
			else
			{
				Vector3* texs = new Vector3[i->second.textureCoords.size()];
				for (UInt j = 0; j < i->second.textureCoords.size(); j++)
				{
					texs[j] = textureCoords[i->second.textureCoords[j] - 1].first;
				}
				newSubMesh->addBuffer(texs);
				delete[] texs;
			}
		}
		if (i->second.normals.size() != 0)
		{
			if (normals[i->second.normals[0]].second == 2)
			{
				Vector2* norms = new Vector2[i->second.normals.size()];
				for (UInt j = 0; j < i->second.normals.size(); j++)
				{
					norms[j] = normals[i->second.normals[j] - 1].first;
				}
				//newSubMesh->addNormals(norms);
				delete[] norms;
			}
			else
			{
				Vector3* norms = new Vector3[i->second.normals.size()];
				for (UInt j = 0; j < i->second.normals.size(); j++)
				{
					norms[j] = vertices[i->second.normals[j] - 1].first;
				}
				//newSubMesh->addNormals(norms);
				delete[] norms;
			}
		}
		newMesh->addSubMesh(newSubMesh);
	}

	return newMesh;
}

Cross::Mesh::Mesh()
{
}

void Cross::Mesh::addSubMesh(SubMesh* subMesh)
{
	subMeshes.push_back(subMesh);
}

Cross::SubMesh* Cross::Mesh::getSubMesh(UInt id)
{
	return subMeshes[id];
}

UInt Cross::Mesh::getSubMeshCount()
{
	return subMeshes.size();
}

void Cross::Mesh::draw()
{
	for (auto i = subMeshes.begin(); i != subMeshes.end(); i++)
	{
		(*i)->draw();
	}
}