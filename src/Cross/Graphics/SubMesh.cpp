#include <Cross/Graphics/SubMesh.h>

Cross::SubMesh::SubMesh(UInt vertexCount)
{
	glGenVertexArrays(1, &vertexArray);

	vertexObjects = 0;

	vertices = vertexCount;

	quads = false;
}

UInt Cross::SubMesh::addBuffer(Vector2* positions, UInt id)
{
	glBindVertexArray(vertexArray);

	UInt object;
	if (id == ~0)
	{
		glGenBuffers(1, &object); // Generate our Vertex Buffer Object
	}
	else
	{
		object = id;
	}

	float* positionData = new float[vertices * 3];
	for (UInt i = 0; i < vertices; i++)
	{
		positionData[i * 2 + 0] = positions[i].x;
		positionData[i * 2 + 1] = positions[i].y;
	}

	glBindBuffer(GL_ARRAY_BUFFER, object); // Bind our Vertex Buffer Object
	glBufferData(GL_ARRAY_BUFFER, (vertices * 2) * sizeof(GLfloat), positionData, GL_STATIC_DRAW); // Set the size and data of our VBO and set it to STATIC_DRAW
	glVertexAttribPointer((GLuint)vertexObjects, 2, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer

	glEnableVertexAttribArray(vertexObjects++);

	delete[] positionData;

	glBindVertexArray(0);

	return object;
}

UInt Cross::SubMesh::addBuffer(Vector3* positions, UInt id)
{
	glBindVertexArray(vertexArray);

	UInt object;
	if (id == ~0)
	{
		glGenBuffers(1, &object); // Generate our Vertex Buffer Object
	}
	else
	{
		object = id;
	}

	float* positionData = new float[vertices * 3];
	for (UInt i = 0; i < vertices; i++)
	{
		positionData[i * 3 + 0] = positions[i].x;
		positionData[i * 3 + 1] = positions[i].y;
		positionData[i * 3 + 2] = positions[i].z;
	}

	glBindBuffer(GL_ARRAY_BUFFER, object); // Bind our Vertex Buffer Object
	glBufferData(GL_ARRAY_BUFFER, (vertices * 3) * sizeof(GLfloat), positionData, GL_STATIC_DRAW); // Set the size and data of our VBO and set it to STATIC_DRAW
	glVertexAttribPointer((GLuint)vertexObjects, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer

	glEnableVertexAttribArray(vertexObjects++);

	delete[] positionData;

	glBindVertexArray(0);

	return object;
}

UInt Cross::SubMesh::addBuffer(Vector4* positions, UInt id)
{
	glBindVertexArray(vertexArray);

	UInt object;
	if (id == ~0)
	{
		glGenBuffers(1, &object); // Generate our Vertex Buffer Object
	}
	else
	{
		object = id;
	}

	float* positionData = new float[vertices * 4];
	for (UInt i = 0; i < vertices; i++)
	{
		positionData[i * 4 + 0] = positions[i].x;
		positionData[i * 4 + 1] = positions[i].y;
		positionData[i * 4 + 2] = positions[i].z;
		positionData[i * 4 + 3] = positions[i].w;
	}

	glBindBuffer(GL_ARRAY_BUFFER, object); // Bind our Vertex Buffer Object
	glBufferData(GL_ARRAY_BUFFER, (vertices * 4) * sizeof(GLfloat), positionData, GL_STATIC_DRAW); // Set the size and data of our VBO and set it to STATIC_DRAW
	glVertexAttribPointer((GLuint)vertexObjects, 4, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer

	glEnableVertexAttribArray(vertexObjects++);

	delete[] positionData;

	glBindVertexArray(0);

	return object;
}

UInt Cross::SubMesh::addBuffer(Color* colors, UInt id)
{
	glBindVertexArray(vertexArray);

	UInt object;
	if (id == ~0)
	{
		glGenBuffers(1, &object); // Generate our Vertex Buffer Object
	}
	else
	{
		object = id;
	}

	float* colorsData = new float[vertices * 3];
	for (UInt i = 0; i < vertices; i++)
	{
		colorsData[i * 3 + 0] = colors[i].r;
		colorsData[i * 3 + 1] = colors[i].g;
		colorsData[i * 3 + 2] = colors[i].b;
	}

	glBindBuffer(GL_ARRAY_BUFFER, object); // Bind our Vertex Buffer Object
	glBufferData(GL_ARRAY_BUFFER, (vertices * 3) * sizeof(GLfloat), colorsData, GL_STATIC_DRAW); // Set the size and data of our VBO and set it to STATIC_DRAW
	glVertexAttribPointer((GLuint)vertexObjects, 3, GL_FLOAT, GL_FALSE, 0, 0); // Set up our vertex attributes pointer

	glEnableVertexAttribArray(vertexObjects++);

	delete[] colorsData;

	glBindVertexArray(0);

	return object;
}

void Cross::SubMesh::setQuads(bool quads)
{
	this->quads = quads;
}

bool Cross::SubMesh::getQuads()
{
	return quads;
}

void Cross::SubMesh::draw()
{
	glBindVertexArray(vertexArray);
	glDrawArrays((quads ? GL_QUADS : GL_TRIANGLES), 0, vertices);
	glBindVertexArray(0);
}