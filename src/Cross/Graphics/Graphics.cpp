#include <Cross/Core.h>
#include <Cross/Window.h>
#include <Cross/Exception.h>
#include <Cross/Graphics/Graphics.h>
#include <Cross/Graphics/Color.h>

Cross::Graphics::Graphics(const Parameters& parameters)
{
	this->parameters = parameters;

#if defined(_WIN32)
	// setup the pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 0;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 0;
	pfd.cRedShift = 0;
	pfd.cGreenBits = 0;
	pfd.cGreenShift = 0;
	pfd.cBlueBits = 0;
	pfd.cBlueShift = 0;
	pfd.cAlphaBits = 0;
	pfd.cAlphaShift = 0;
	pfd.cAccumBits = 0;
	pfd.cAccumRedBits = 0;
	pfd.cAccumGreenBits = 0;
	pfd.cAccumBlueBits = 0;
	pfd.cAccumAlphaBits = 0;
	pfd.cDepthBits = 32;
	pfd.cStencilBits = 0;
	pfd.cAuxBuffers = 0;
	pfd.iLayerType = PFD_MAIN_PLANE;
	pfd.bReserved = 0;
	pfd.dwLayerMask = 0;
	pfd.dwVisibleMask = 0;
	pfd.dwDamageMask = 0;

	// get the device context handle for the window
	deviceContext = GetDC(parameters.window->getHandle());
	if (!deviceContext)
	{
		Ex("Failed to get the device context from the window.");
	}

	// set the pixel format
	pixelFormat = ChoosePixelFormat(deviceContext, &pfd);
	if (!SetPixelFormat(deviceContext, pixelFormat, &pfd))
	{
		Ex("Failed to set the pixel format.");
	}

	// create opengl render context and make it current
	renderContext = wglCreateContext(deviceContext);
	if (!renderContext)
	{
		Ex("Failed to create the rendering context.");
	}
	if (!wglMakeCurrent(deviceContext, renderContext))
	{
		Ex("Failed to apply the current rendering context.");
	}
#elif defined(unix)

	/*GLXFBConfig* fbConfigs;
			const int fbAttribs[] = {
					GLX_RENDER_TYPE, GLX_RGBA_BIT,
					GLX_X_RENDERABLE, true,
					GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
					GLX_DOUBLEBUFFER, true,
					GLX_RED_SIZE, 8,
					GLX_BLUE_SIZE, 8,
					GLX_GREEN_SIZE, 8,
					0 };
	GLint attribs[] = { GLX_CONTEXT_MAJOR_VERSION_ARB, 3, GLX_CONTEXT_MINOR_VERSION_ARB, 3, 0 };
	GLXContext fuck = glXCreateContextAttribsARB(parameters.window->getDisplay(), fbConfigs[0], 0, true, attribs);
	glXMakeCurrent(parameters.window->getDisplay(), parameters.window->getWindow(), fuck);*/

#endif
	// initiate glew
	GLenum error = glewInit(); // Enable GLEW
	if (error != GLEW_OK) // If GLEW fails
	{
		Ex("Failed to initiate glew.");
	}

	// setup stuff
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);

	/*glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0);
	glDepthFunc(GL_LEQUAL);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glAlphaFunc(GL_GREATER,0.1);
	glEnable(GL_CULL_FACE);*/

	glEnable(GL_ALPHA_TEST);
	glEnable(GL_TEXTURE_2D);
}

Cross::Graphics::~Graphics()
{
}

void Cross::Graphics::begin() const
{
}

void Cross::Graphics::end() const
{
}

void Cross::Graphics::present() const
{
	glFlush();
	glFinish();
#if defined(_WIN32)
	SwapBuffers(deviceContext);
#elif defined(unix)
	glXSwapBuffers(parameters.window->getDisplay(), parameters.window->getWindow());
#endif
}

void Cross::Graphics::clear() const
{
	glClearColor(0, 0, 0, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Cross::Graphics::clear(const Color& color) const
{
	glClearColor(color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}
