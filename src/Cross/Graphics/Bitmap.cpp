#include <Cross/Graphics/Bitmap.h>
#include <Cross/Utility/File.h>
#include <Cross/Utility/Misc.h>
#include <Cross/Exception.h>
#include <iostream>

Cross::Bitmap::Bitmap(const std::string& fileName)
{
	UByte* fileData;
	UInt fileSize;
	if (!File::getData(fileName, &fileData, &fileSize))
	{
		Ex("Failed to open file: " + fileName);
	}
	if (fileData[0] != 'B' || fileData[1] != 'M')
	{
		Ex(fileName + " is not a valid bitmap.");
	}
	UInt dataSize = build<UInt>(fileData + 2);
	UInt dataStart = build<UInt>(fileData + 10);
	width = build<UInt>(fileData + 18);
	height = build<UInt>(fileData + 22);
	short bits = build<UInt>(fileData + 28);
	data = new Cross::Color[width * height];
	UInt dataPos = dataStart;
	//for (int h = height - 1; h >= 0; h--)
	for (unsigned int h = 0; h < height; h++)
	{
		for (unsigned int w = 0; w < width; w++)
		{
			if (bits == 24)
			{
				data[h * width + w].b = fileData[dataPos++];
				data[h * width + w].g = fileData[dataPos++];
				data[h * width + w].r = fileData[dataPos++];
			}
			if (bits == 32)
			{
				data[h * width + w].b = fileData[dataPos++];
				data[h * width + w].g = fileData[dataPos++];
				data[h * width + w].r = fileData[dataPos++];
				data[h * width + w].a = fileData[dataPos++];
			}
		}
	}
}