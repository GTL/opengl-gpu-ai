#include <Cross/Graphics/Texture.h>
#include <Cross/Utility/File.h>
#include <Cross/Graphics/Bitmap.h>

Cross::Texture::Texture(const std::string& fileName)
{
	Bitmap lulz(fileName);

	UByte* data = new UByte[lulz.getWidth() * lulz.getHeight() * 4];
	//File::getData(fileName, &data);
	UInt pos = 0;
	Color* colahs = new Color[lulz.getWidth() * lulz.getHeight()];
	lulz.getData(colahs);
	for (UInt i = 0; i < lulz.getWidth() * lulz.getHeight(); i++)
	{
		data[pos++] = colahs[i].r;
		data[pos++] = colahs[i].g;
		data[pos++] = colahs[i].b;
		data[pos++] = colahs[i].a;
	}
	delete[] colahs;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, lulz.getWidth(), lulz.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	delete[] data;
}

Cross::Texture::~Texture()
{
	glDeleteTextures(1, &texture);
}

UInt Cross::Texture::getTexture()
{
	return texture;
}
