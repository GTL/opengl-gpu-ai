#include <Cross/Graphics/Image.h>

Cross::Image::Image()
{
	data = nullptr;
	width = height = 0;
}

Cross::Image::~Image()
{
	if (data != nullptr)
	{
		delete[] data;
	}
}

void Cross::Image::getData(Cross::Color* data)
{
	for (UInt i = 0; i < width * height; i++)
	{
		data[i] = this->data[i];
	}
}

void Cross::Image::setData(Cross::Color* data)
{
	for (UInt i = 0; i < width * height; i++)
	{
		this->data[i] = data[i];
	}
}

UInt Cross::Image::getWidth()
{
	return width;
}

UInt Cross::Image::getHeight()
{
	return height;
}