#include <Cross/Window.h>
#include <Cross/Exception.h>
#include <Cross/Math/Vector2.h>
#include <iostream>

#if defined(_WIN32)
std::map<HWND, Cross::Window*> Cross::Window::handleMap;

LRESULT CALLBACK Cross::Window::winProc(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		DestroyWindow(window);
		handleMap[window]->handle = false;
		return 0;
	case WM_DESTROY:
		return 0;
	}
	return DefWindowProc(window, message, wParam, lParam);
}
#endif

Cross::Window::Window(const Window& copy)
{
}

Cross::Window::Window(const Class& windowClass)
{
#if defined(_WIN32)
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = winProc; // function pointer to winproc
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandle(nullptr); // application instance
	wc.hIcon = LoadIcon(0, IDI_APPLICATION); // use application icon
	wc.hCursor = LoadCursor(0, IDC_ARROW); // use arrow cursor
	wc.hbrBackground = CreateSolidBrush(RGB(windowClass.background.r, windowClass.background.g, windowClass.background.b)); // background color
	wc.lpszMenuName = 0; // no menu
	wc.lpszClassName = windowClass.name.c_str(); // window class name 

	// Attempt to register the window class
	if (!RegisterClass(&wc))
	{
		//Ex("Failed to register the window class.");
	}

	// Setup the style
	style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
	style = WS_OVERLAPPEDWINDOW;

	// Create the window
	RECT R = {0, 0, windowClass.width, windowClass.height};
	AdjustWindowRect(&R, style, false);
	if (!(handle = CreateWindow(windowClass.name.c_str(), "", style, 50, 50, R.right - R.left, R.bottom - R.top, nullptr, nullptr, GetModuleHandle(nullptr), nullptr)))
	{
		//Ex("Failed to create the application window.");
	}

	// Update the window (...or something, the function name isn't very clear)
	UpdateWindow(handle);

	// Add to the handle map
	handleMap.insert(std::make_pair(handle, this));
#elif defined(unix)
	display = XOpenDisplay(nullptr);
	if (display == nullptr)
	{
		Ex("Failed to connect to the display.");
	}

	//XSetWindowAttributes windowAttributes;
	Int winmask;
	Int nMajorVer = 0;
	Int nMinorVer = 0;
	GLXFBConfig* fbConfigs;
	int numConfigs = 0;
	static int fbAttribs[] = {
		GLX_RENDER_TYPE,   GLX_RGBA_BIT,
		GLX_X_RENDERABLE,  True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_DOUBLEBUFFER,  True,
		GLX_RED_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_GREEN_SIZE, 8,
	0 };

	// Get Version info
	glXQueryVersion(display, &nMajorVer, &nMinorVer);

	if(nMajorVer == 1 && nMinorVer < 2)
	{
		Ex("GLX 1.2 or greater is necessary.");
	}

	// get a new fb config that meets our attrib requirements
	fbConfigs = glXChooseFBConfig(display, DefaultScreen(display), fbAttribs, &numConfigs);
	visual = glXGetVisualFromFBConfig(display, fbConfigs[0]);

	// now create an X window
	windowAttributes.event_mask = ExposureMask | VisibilityChangeMask |
	KeyPressMask | PointerMotionMask | StructureNotifyMask | ButtonPressMask | ButtonReleaseMask |
	KeyReleaseMask;

	windowAttributes.border_pixel = 0;
	windowAttributes.bit_gravity = StaticGravity;
	windowAttributes.colormap = XCreateColormap(display,
	RootWindow(display, visual->screen),
	visual->visual, AllocNone);
	winmask = CWBorderPixel | CWBitGravity | CWEventMask| CWColormap;

	window = XCreateWindow(display, DefaultRootWindow(display), 20, 20,
	windowClass.width, windowClass.height, 0,
	visual->depth, InputOutput,
	visual->visual, winmask, &windowAttributes);

	Atom wmDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(display, window, &wmDelete, 1);

	XMapWindow(display, window);

	// also create a new GL context for rendering
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
		GLX_CONTEXT_MINOR_VERSION_ARB, 2,
	0 };
	context = glXCreateContextAttribsARB(display, fbConfigs[0], 0, true, attribs);
	glXMakeCurrent(display, window, context);
#endif
}

Cross::Window::~Window()
{
}

#if defined(_WIN32)
HWND Cross::Window::getHandle() const
{
	return handle;
}

DWORD Cross::Window::getStyle() const
{
	return style;
}
#elif defined(unix)
::Window Cross::Window::getWindow() const
{
	return window;
}

_XDisplay* Cross::Window::getDisplay() const
{
	return display;
}

GLXContext Cross::Window::getContext() const
{
	return context;
}

std::vector<XEvent> Cross::Window::getEvents() const
{
	return events;
}
#endif

void Cross::Window::update()
{
#if defined(_WIN32)
	MSG msg;
	msg.message = WM_NULL;
	while (PeekMessage(&msg, handle, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
#elif defined(unix)
	events.clear();
	while (XPending(display))
	{
		XEvent event;
		XNextEvent(display, &event);
		events.push_back(event);
	}
	for (unsigned int i = 0; i < events.size(); i++)
	{
		XEvent& event = events[i];
		bool first = (i == 0);
		bool last = (i == events.size() - 1);
		switch (event.type)
		{
		case 33:
			XDestroyWindow(display, window);
			break;
		case DestroyNotify:
			window = nullptr;
			display = nullptr;
			break;
		case ButtonPress:
			//std::cout << "lulz" << std::endl;
			break;
		case ButtonRelease:
			//std::cout << "HAH!" << std::endl;
			break;
		case KeyPress:
			if (first || events[i - 1].type != KeyRelease)
			{
				//std::cout << "Key pressed: " << event.xbutton.button << std::endl;
			}
			break;
		case KeyRelease:
			if (last || events[i + 1].type != KeyPress)
			{
				//std::cout << "Key released: " << event.xbutton.button << std::endl;
			}
			break;
		}
	}
#endif
}

void Cross::Window::show() const
{
#if defined(_WIN32)
	ShowWindow(handle, SW_SHOW);
#endif
}

void Cross::Window::hide() const
{
#if defined(_WIN32)
	ShowWindow(handle, SW_HIDE);
#endif
}

void Cross::Window::focus() const
{
#if defined(_WIN32)
	SetForegroundWindow(handle);
#endif
}

void Cross::Window::setWidth(const unsigned int width) const
{
#if defined(_WIN32)
	RECT rect = {0, 0, width, getHeight()};
	AdjustWindowRect(&rect, style, false);
	SetWindowPos(handle, nullptr, 0, 0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE | SWP_NOZORDER);
#endif
}

void Cross::Window::setHeight(const unsigned int height) const
{
#if defined(_WIN32)
	RECT rect = {0, 0, getWidth(), height};
	AdjustWindowRect(&rect, style, false);
	SetWindowPos(handle, nullptr, 0, 0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE | SWP_NOZORDER);
#endif
}

unsigned int Cross::Window::getWidth() const
{
#if defined(_WIN32)
	RECT rect = {0, 0, 0, 0};
	//AdjustWindowRectEx(&rect, style, false, extendedStyle);
	AdjustWindowRect(&rect, style, false);
	int borders = -rect.left + rect.right;
	GetWindowRect(handle, &rect);
	return rect.right - rect.left - borders;
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(display, window, &attributes);
	return attributes.width;
#endif
}

unsigned int Cross::Window::getHeight() const
{
#if defined(_WIN32)
	RECT rect = {0, 0, 0, 0};
	//AdjustWindowRectEx(&rect, style, false, extendedStyle);
	AdjustWindowRect(&rect, style, false);
	int borders = -rect.top + rect.bottom;
	GetWindowRect(handle, &rect);
	return rect.bottom - rect.top - borders;
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(display, window, &attributes);
	return attributes.height;
#endif
}

Vector2 Cross::Window::getPosition() const
{
#if defined(_WIN32)
	RECT windowRect;
	windowRect.bottom = windowRect.left = windowRect.right = windowRect.top = 0;
	GetWindowRect(handle, &windowRect);
	return Vector2((float)windowRect.left, (float)windowRect.top);
#elif defined(unix)
	return Vector2(0, 0);
#endif
}

void Cross::Window::setTitle(const std::string& title) const
{
#if defined(_WIN32)
	SetWindowText(handle, title.c_str());
#elif defined(unix)
	XChangeProperty(display, window, XInternAtom(display, "_NET_WM_NAME", false), XInternAtom(display, "UTF8_STRING", false), 8, PropModeReplace, (unsigned char*)title.c_str(), title.length());
#endif
}

std::string Cross::Window::getTitle() const
{
#if defined(_WIN32)
	char buff[256];
	GetWindowText(handle, buff, 256);
	return std::string(buff);
#elif defined(unix)
	return "";
#endif
}

bool Cross::Window::isDestroyed()
{
#if defined(_WIN32)
	return handle == nullptr;
#elif defined(unix)
	return window == nullptr || display == nullptr;
#endif
}
