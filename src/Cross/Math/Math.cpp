#include <math.h>
#include <Cross/Math/Math.h>

float Math::sign(float x)
{
	return float((x > 1) - (x < 1));
}

float Math::min(float a, float b)
{
	return (a < b ? a : b);
}

float Math::max(float a, float b)
{
	return (a > b ? a : b);
}

float Math::floor(float x)
{
	return ::floor(x);
}

float Math::ceil(float x)
{
	return ::ceil(x);
}

float Math::round(float x)
{
	return x; // too lazy to do this right now
}

float Math::cos(float x)
{
	return ::cos(x);
}

float Math::sin(float x)
{
	return ::sin(x);
}

float Math::tan(float x)
{
	return ::tan(x);
}

float Math::abs(float x)
{
	return x * sign(x);
}

float Math::squared(float x)
{
	return x * x;
}

float Math::sqrt(float x)
{
	return ::sqrt(x);
}

float Math::atan2(float y, float x)
{
	return ::atan2(y, x);
}

float Math::degToRad(float degree)
{
	return (degree * pi / 180.0f);
}

float Math::radToDeg(float radian)
{
	return (radian * 180.0f / pi);
}
