#include <Cross/Math/Matrix.h>
#include <Cross/Math/Vector2.h>
#include <Cross/Math/Vector3.h>
#include <Cross/Math/Vector4.h>
#include <Cross/Math/Math.h>

const Matrix Matrix::zero(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
const Matrix Matrix::identity(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1);

Matrix Matrix::inverse(const Matrix& matrix)
{
	Matrix result;
	result.m11 = matrix.m22 * matrix.m33 * matrix.m44 + matrix.m23 * matrix.m34 * matrix.m42 + matrix.m24 * matrix.m32 * matrix.m43 - matrix.m22 * matrix.m34 * matrix.m43 - matrix.m23 * matrix.m32 * matrix.m44 - matrix.m24 * matrix.m33 * matrix.m42;
	result.m12 = matrix.m12 * matrix.m34 * matrix.m43 + matrix.m13 * matrix.m32 * matrix.m44 + matrix.m14 * matrix.m33 * matrix.m42 - matrix.m12 * matrix.m33 * matrix.m44 - matrix.m13 * matrix.m34 * matrix.m42 - matrix.m14 * matrix.m32 * matrix.m43;
	result.m13 = matrix.m12 * matrix.m23 * matrix.m44 + matrix.m13 * matrix.m24 * matrix.m42 + matrix.m14 * matrix.m22 * matrix.m43 - matrix.m12 * matrix.m24 * matrix.m43 - matrix.m13 * matrix.m22 * matrix.m44 - matrix.m14 * matrix.m23 * matrix.m42;
	result.m14 = matrix.m12 * matrix.m24 * matrix.m33 + matrix.m13 * matrix.m22 * matrix.m34 + matrix.m14 * matrix.m23 * matrix.m32 - matrix.m12 * matrix.m23 * matrix.m34 - matrix.m13 * matrix.m24 * matrix.m32 - matrix.m14 * matrix.m22 * matrix.m33;
	result.m21 = matrix.m21 * matrix.m34 * matrix.m43 + matrix.m23 * matrix.m31 * matrix.m44 + matrix.m24 * matrix.m33 * matrix.m41 - matrix.m21 * matrix.m33 * matrix.m44 - matrix.m23 * matrix.m34 * matrix.m41 - matrix.m24 * matrix.m31 * matrix.m43;
	result.m22 = matrix.m11 * matrix.m33 * matrix.m44 + matrix.m13 * matrix.m34 * matrix.m41 + matrix.m14 * matrix.m31 * matrix.m43 - matrix.m11 * matrix.m34 * matrix.m43 - matrix.m13 * matrix.m31 * matrix.m44 - matrix.m14 * matrix.m33 * matrix.m41;
	result.m23 = matrix.m11 * matrix.m24 * matrix.m43 + matrix.m13 * matrix.m21 * matrix.m44 + matrix.m14 * matrix.m23 * matrix.m41 - matrix.m11 * matrix.m23 * matrix.m44 - matrix.m13 * matrix.m24 * matrix.m41 - matrix.m14 * matrix.m21 * matrix.m43;
	result.m24 = matrix.m11 * matrix.m23 * matrix.m34 + matrix.m13 * matrix.m24 * matrix.m31 + matrix.m14 * matrix.m21 * matrix.m33 - matrix.m11 * matrix.m24 * matrix.m33 - matrix.m13 * matrix.m21 * matrix.m34 - matrix.m14 * matrix.m23 * matrix.m31;
	result.m31 = matrix.m21 * matrix.m32 * matrix.m44 + matrix.m22 * matrix.m34 * matrix.m41 + matrix.m24 * matrix.m31 * matrix.m42 - matrix.m21 * matrix.m34 * matrix.m42 - matrix.m22 * matrix.m31 * matrix.m44 - matrix.m24 * matrix.m32 * matrix.m41;
	result.m32 = matrix.m11 * matrix.m34 * matrix.m42 + matrix.m12 * matrix.m31 * matrix.m44 + matrix.m14 * matrix.m32 * matrix.m41 - matrix.m11 * matrix.m32 * matrix.m44 - matrix.m12 * matrix.m34 * matrix.m41 - matrix.m14 * matrix.m31 * matrix.m42;
	result.m33 = matrix.m11 * matrix.m22 * matrix.m44 + matrix.m12 * matrix.m24 * matrix.m41 + matrix.m14 * matrix.m21 * matrix.m42 - matrix.m11 * matrix.m24 * matrix.m42 - matrix.m12 * matrix.m21 * matrix.m44 - matrix.m14 * matrix.m22 * matrix.m41;
	result.m34 = matrix.m11 * matrix.m24 * matrix.m32 + matrix.m12 * matrix.m21 * matrix.m34 + matrix.m14 * matrix.m22 * matrix.m31 - matrix.m11 * matrix.m22 * matrix.m34 - matrix.m12 * matrix.m24 * matrix.m31 - matrix.m14 * matrix.m21 * matrix.m32;
	result.m41 = matrix.m21 * matrix.m33 * matrix.m42 + matrix.m22 * matrix.m31 * matrix.m43 + matrix.m23 * matrix.m32 * matrix.m41 - matrix.m21 * matrix.m32 * matrix.m43 - matrix.m22 * matrix.m33 * matrix.m41 - matrix.m23 * matrix.m31 * matrix.m42;
	result.m42 = matrix.m11 * matrix.m32 * matrix.m43 + matrix.m12 * matrix.m33 * matrix.m41 + matrix.m13 * matrix.m31 * matrix.m42 - matrix.m11 * matrix.m33 * matrix.m42 - matrix.m12 * matrix.m31 * matrix.m43 - matrix.m13 * matrix.m32 * matrix.m41;
	result.m43 = matrix.m11 * matrix.m23 * matrix.m42 + matrix.m12 * matrix.m21 * matrix.m43 + matrix.m13 * matrix.m22 * matrix.m41 - matrix.m11 * matrix.m22 * matrix.m43 - matrix.m12 * matrix.m23 * matrix.m41 - matrix.m13 * matrix.m21 * matrix.m42;
	result.m44 = matrix.m11 * matrix.m22 * matrix.m33 + matrix.m12 * matrix.m23 * matrix.m31 + matrix.m13 * matrix.m21 * matrix.m32 - matrix.m11 * matrix.m23 * matrix.m32 - matrix.m12 * matrix.m21 * matrix.m33 - matrix.m13 * matrix.m22 * matrix.m31;
	result /= matrix.getDeterminant();
	return result;
}

Matrix Matrix::transpose(const Matrix& matrix)
{
	return Matrix(matrix.m11, matrix.m21, matrix.m31, matrix.m41, matrix.m12, matrix.m22, matrix.m32, matrix.m42,
				  matrix.m13, matrix.m23, matrix.m33, matrix.m43, matrix.m14, matrix.m24, matrix.m34, matrix.m44);
}

Matrix Matrix::makeTranslation(const Vector2& translation)
{
	return Matrix(1,             0,             0, 0,
				  0,             1,             0, 0,
				  0,             0,             1, 0,
				  translation.x, translation.y, 0, 1);
}

Matrix Matrix::makeTranslation(const Vector3& translation)
{
	return Matrix(1,             0,             0,             0,
				  0,             1,             0,             0,
				  0,             0,             1,             0,
				  translation.x, translation.y, translation.z, 1);
}

Matrix Matrix::makeScaling(const Vector2& scaling)
{
	return Matrix(scaling.x, 0,         0,
				  0,         scaling.y, 0,
				  0,         0,         1);
}

Matrix Matrix::makeScaling(const Vector3& scaling)
{
	return Matrix(scaling.x, 0,         0,         0,
				  0,         scaling.y, 0,         0,
				  0,         0,         scaling.z, 0,
				  0,         0,         0,         1);
}

Matrix Matrix::makeRotation(const float angle)
{
	return Matrix(Math::cos(angle), Math::sin(angle),  0,
				  -Math::sin(angle), Math::cos(angle), 0,
				  0,                0,                 1);
}

Matrix Matrix::makeRotationX(const float angle)
{
	return Matrix(1, 0,          0,           0,
				  0, Math::cos(angle),  Math::sin(angle), 0,
				  0, -Math::sin(angle), Math::cos(angle), 0,
				  0, 0,                0,                 1);
}

Matrix Matrix::makeRotationY(const float angle)
{
	return Matrix(Math::cos(angle), 0, -Math::sin(angle), 0,
				  0,                1, 0,                 0,
				  Math::sin(angle), 0, Math::cos(angle),  0,
				  0,                0, 0,                 1);
}

Matrix Matrix::makeRotationZ(const float angle)
{
	return Matrix(Math::cos(angle),  Math::sin(angle), 0, 0,
				  -Math::sin(angle), Math::cos(angle), 0, 0,
				  0,                0,                 1, 0,
				  0,                0,                 0, 1);
}

Matrix Matrix::makePerspective(const float fovY, const float aspectRatio, const float zNear, const float zFar)
{
	float yScale = 1.0f / Math::tan(fovY / 2.0f);
	float xScale = yScale / aspectRatio;
	return Matrix(xScale, 0,      0,                              0,
				  0,      yScale, 0,                              0,
				  0,      0,      zFar / (zFar - zNear),          1,
				  0,      0,      -zNear * zFar / (zFar - zNear), 0);
}

Matrix Matrix::makeLookAt(const Vector3& eye, const Vector3& at, const Vector3& up)
{
	/*Vector3 zAxis = Vector3::normalized(at - eye);
	Vector3 xAxis = Vector3::normalized(Vector3::crossProduct(up, zAxis));
	Vector3 yAxis = Vector3::crossProduct(zAxis, xAxis);
	return Matrix(xAxis.x,                          yAxis.x,                          zAxis.x,                           0,
				  xAxis.y,                          yAxis.y,                          zAxis.y,                           0,
				  xAxis.z,                          yAxis.z,                          zAxis.z,                           0,
				  Vector3::dotProduct(xAxis, eye),  Vector3::dotProduct(yAxis, eye),  Vector3::dotProduct(zAxis, eye),   1);*/

	Vector3 zAxis = Vector3::normalized(at - eye);
	Vector3 xAxis = Vector3::normalized(Vector3::crossProduct(up, zAxis));
	Vector3 yAxis = Vector3::crossProduct(zAxis, xAxis);
	return Matrix(xAxis.x,                          yAxis.x,                          zAxis.x,                           0,
				  xAxis.y,                          yAxis.y,                          zAxis.y,                           0,
				  xAxis.z,                          yAxis.z,                          zAxis.z,                           0,
				  -Vector3::dotProduct(xAxis, eye), -Vector3::dotProduct(yAxis, eye), -Vector3::dotProduct(zAxis, eye),  1);
}

Matrix Matrix::makeOrtho(float left, float top, float right, float bottom)
{
	Vector3 t((right + left) / (right - left), (top + bottom) / (top - bottom), 0);
	return Matrix(-2 / (right - left), 0,                   0,   0,
				  0,                   -2 / (top - bottom), 0,   0,
				  0,                   0,                   1,   0,
				  t.x,                 t.y,                 t.z, 1);
}

Matrix::Matrix()
{
	m11 = m12 = m13 = m14 = m21 = m22 = m23 = m24 = m31 = m32 = m33 = m34 = m41 = m42 = m43 = m44 = 0;
}

Matrix::Matrix(const float m11, const float m12, const float m21, const float m22)
{
	this->m11 = m11; this->m12 = m12; this->m13 = 0; this->m14 = 0;
	this->m21 = m21; this->m22 = m22; this->m23 = 0; this->m24 = 0;
	this->m31 =   0; this->m32 =   0; this->m33 = 1; this->m34 = 0;
	this->m41 =   0; this->m42 =   0; this->m43 = 0; this->m44 = 1;
}

Matrix::Matrix(const float m11, const float m12, const float m13, const float m21, const float m22, const float m23, const float m31, const float m32, const float m33)
{
	this->m11 = m11; this->m12 = m12; this->m13 = m13; this->m14 = 0;
	this->m21 = m21; this->m22 = m22; this->m23 = m23; this->m24 = 0;
	this->m31 = m31; this->m32 = m32; this->m33 = m33; this->m34 = 0;
	this->m41 =   0; this->m42 =   0; this->m43 =   0; this->m44 = 1;
}

Matrix::Matrix(const float m11, const float m12, const float m13, const float m14, const float m21, const float m22, const float m23, const float m24,
			   const float m31, const float m32, const float m33, const float m34, const float m41, const float m42, const float m43, const float m44)
{
	this->m11 = m11; this->m12 = m12; this->m13 = m13; this->m14 = m14;
	this->m21 = m21; this->m22 = m22; this->m23 = m23; this->m24 = m24;
	this->m31 = m31; this->m32 = m32; this->m33 = m33; this->m34 = m34;
	this->m41 = m41; this->m42 = m42; this->m43 = m43; this->m44 = m44;
}

Matrix::Matrix(const Matrix& copy)
{
	m11 = copy.m11; m12 = copy.m12; m13 = copy.m13; m14 = copy.m14;
	m21 = copy.m21; m22 = copy.m22; m23 = copy.m23; m24 = copy.m24;
	m31 = copy.m31; m32 = copy.m32; m33 = copy.m33; m34 = copy.m34;
	m41 = copy.m41; m42 = copy.m42; m43 = copy.m43; m44 = copy.m44;
}

void Matrix::operator=(const Matrix& operand)
{
	m11 = operand.m11; m12 = operand.m12; m13 = operand.m13; m14 = operand.m14;
	m21 = operand.m21; m22 = operand.m22; m23 = operand.m23; m24 = operand.m24;
	m31 = operand.m31; m32 = operand.m32; m33 = operand.m33; m34 = operand.m34;
	m41 = operand.m41; m42 = operand.m42; m43 = operand.m43; m44 = operand.m44;
}

float* Matrix::operator[](const unsigned int index)
{
	return m[index];
}

Matrix Matrix::operator*(const Matrix& operand) const
{
	return Matrix(m11 * operand.m11 + m12 * operand.m21 + m13 * operand.m31 + m14 * operand.m41,
				  m11 * operand.m12 + m12 * operand.m22 + m13 * operand.m32 + m14 * operand.m42,
				  m11 * operand.m13 + m12 * operand.m23 + m13 * operand.m33 + m14 * operand.m43,
				  m11 * operand.m14 + m12 * operand.m24 + m13 * operand.m34 + m14 * operand.m44,
				  m21 * operand.m11 + m22 * operand.m21 + m23 * operand.m31 + m24 * operand.m41,
				  m21 * operand.m12 + m22 * operand.m22 + m23 * operand.m32 + m24 * operand.m42,
				  m21 * operand.m13 + m22 * operand.m23 + m23 * operand.m33 + m24 * operand.m43,
				  m21 * operand.m14 + m22 * operand.m24 + m23 * operand.m34 + m24 * operand.m44,
				  m31 * operand.m11 + m32 * operand.m21 + m33 * operand.m31 + m34 * operand.m41,
				  m31 * operand.m12 + m32 * operand.m22 + m33 * operand.m32 + m34 * operand.m42,
				  m31 * operand.m13 + m32 * operand.m23 + m33 * operand.m33 + m34 * operand.m43,
				  m31 * operand.m14 + m32 * operand.m24 + m33 * operand.m34 + m34 * operand.m44,
				  m41 * operand.m11 + m42 * operand.m21 + m43 * operand.m31 + m44 * operand.m41,
				  m41 * operand.m12 + m42 * operand.m22 + m43 * operand.m32 + m44 * operand.m42,
				  m41 * operand.m13 + m42 * operand.m23 + m43 * operand.m33 + m44 * operand.m43,
				  m41 * operand.m14 + m42 * operand.m24 + m43 * operand.m34 + m44 * operand.m44);
}

Matrix& Matrix::operator*=(const Matrix& operand)
{
	*this = *this * operand;
	return *this;
}

Matrix& Matrix::operator*=(const float operand)
{
	m11 *= operand; m12 *= operand; m13 *= operand; m14 *= operand;
	m21 *= operand; m22 *= operand; m23 *= operand; m24 *= operand;
	m31 *= operand; m32 *= operand; m33 *= operand; m34 *= operand;
	m41 *= operand; m42 *= operand; m43 *= operand; m44 *= operand;
	return *this;
}

Matrix& Matrix::operator/=(const float operand)
{
	float inverse = 1 / operand;
	m11 *= inverse; m12 *= inverse; m13 *= inverse; m14 *= inverse;
	m21 *= inverse; m22 *= inverse; m23 *= inverse; m24 *= inverse;
	m31 *= inverse; m32 *= inverse; m33 *= inverse; m34 *= inverse;
	m41 *= inverse; m42 *= inverse; m43 *= inverse; m44 *= inverse;
	return *this;
}

float Matrix::getDeterminant() const
{
	return m11 * m22 * m33 * m44 + m11 * m23 * m34 * m42 + m11 * m24 * m32 * m43 +
		   m12 * m21 * m34 * m43 + m12 * m23 * m31 * m44 + m12 * m24 * m33 * m41 +
		   m13 * m21 * m32 * m44 + m13 * m22 * m34 * m41 + m13 * m24 * m31 * m42 +
		   m14 * m21 * m33 * m42 + m14 * m22 * m31 * m43 + m14 * m23 * m32 * m41 -
		   m11 * m22 * m34 * m43 - m11 * m23 * m32 * m44 - m11 * m24 * m33 * m42 -
		   m12 * m21 * m33 * m44 - m12 * m23 * m34 * m41 - m12 * m24 * m31 * m43 -
		   m13 * m21 * m34 * m42 - m13 * m22 * m31 * m44 - m13 * m24 * m32 * m41 -
		   m14 * m21 * m32 * m43 - m14 * m22 * m33 * m41 - m14 * m23 * m31 * m42;
}

const float* Matrix::getData() const
{
	return (float*)m;
}
