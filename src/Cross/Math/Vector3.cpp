#include <Cross/Math/Vector3.h>
#include <Cross/Math/Vector2.h>
#include <Cross/Math/Math.h>

const Vector3 Vector3::zero = Vector3(0, 0, 0);
const Vector3 Vector3::one = Vector3(1, 1, 1);

float Vector3::distance(const Vector3& vectorA, const Vector3& vectorB)
{
	return Math::sqrt((vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y) + (vectorA.z - vectorB.z) * (vectorA.z - vectorB.z));
}

float Vector3::distanceSquared(const Vector3& vectorA, const Vector3& vectorB)
{
	return (vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y) + (vectorA.z - vectorB.z) * (vectorA.z - vectorB.z);
}

float Vector3::distanceManhattan(const Vector3& vectorA, const Vector3& vectorB)
{
	return Math::abs(vectorA.x - vectorB.x) + Math::abs(vectorA.y - vectorB.y) + Math::abs(vectorA.z - vectorB.z);
}

Vector3 Vector3::normalized(const Vector3& vector)
{
	Vector3 result = vector;
	result.normalize();
	return result;
}

float Vector3::dotProduct(const Vector3& vectorA, const Vector3& vectorB)
{
	return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y) + (vectorA.z * vectorB.z);
}

Vector3 Vector3::crossProduct(const Vector3& vectorA, const Vector3& vectorB)
{
	return Vector3(vectorA.y * vectorB.z - vectorA.z * vectorB.y, vectorA.z * vectorB.x - vectorA.x * vectorB.z, vectorA.x * vectorB.y - vectorA.y * vectorB.x);
}

void orthonormalBasis(Vector3* vectorA, Vector3* vectorB, Vector3* vectorC)
{
	if (vectorA->getLengthManhattan() == 0)
	{
		return;
	}
	vectorA->normalize();
	vectorB->set(1, 0, 0);
	*vectorC = *vectorA % *vectorB;
	if (vectorC->getLengthManhattan() == 0)
	{
		vectorB->set(0, 1, 0);
		*vectorC = *vectorA % *vectorB;
	}
	*vectorB = *vectorA % *vectorC;
	vectorB->normalize();
	vectorC->normalize();
}

Vector3::Vector3()
{
	x = y = z = 0;
}

Vector3::Vector3(const Vector3& copy)
{
	x = copy.x;
	y = copy.y;
	z = copy.z;
}

Vector3::Vector3(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3::operator Vector2() const
{
	return Vector2(x, y);
}

Vector3& Vector3::operator=(const Vector3& operand)
{
	x = operand.x;
	y = operand.y;
	z = operand.z;
	return *this;
}

float& Vector3::operator[](unsigned int index)
{
	return m[index];
}

const float Vector3::operator[](unsigned int index) const
{
	return m[index];
}

Vector3 Vector3::operator*(const float operand) const
{
	return Vector3(x * operand, y * operand, z * operand);
}

Vector3& Vector3::operator*=(const float operand)
{
	this->x *= operand;
	this->y *= operand;
	this->z *= operand;
	return *this;
}

Vector3 Vector3::operator/(const float operand) const
{
	return Vector3(x / operand, y / operand, z / operand);
}

Vector3& Vector3::operator/=(const float operand)
{
	this->x /= operand;
	this->y /= operand;
	this->z /= operand;
	return *this;
}

Vector3 Vector3::operator+(const Vector3& operand) const
{
	return Vector3(x + operand.x, y + operand.y, z + operand.z);
}

Vector3& Vector3::operator+=(const Vector3& operand)
{
	this->x += operand.x;
	this->y += operand.y;
	this->z += operand.z;
	return *this;
}

Vector3 Vector3::operator-() const
{
	return Vector3(-x, -y, -z);
}

Vector3 Vector3::operator-(const Vector3& operand) const
{
	return Vector3(x - operand.x, y - operand.y, z - operand.z);
}

Vector3& Vector3::operator-=(const Vector3& operand)
{
	this->x -= operand.x;
	this->y -= operand.y;
	this->z -= operand.z;
	return *this;
}

Vector3 Vector3::operator%(const Vector3& operand) const
{
	return Vector3(y * operand.z - z * operand.y, z * operand.x - x * operand.z, x * operand.y - y * operand.x);
}

Vector3& Vector3::operator%=(const Vector3& operand)
{
	*this = *this % operand;
	return *this;
}

bool Vector3::operator==(const Vector3& operand) const
{
	return (x == operand.x && y == operand.y && z == operand.z);
}

bool Vector3::operator!=(const Vector3& operand) const
{
	return (x != operand.x || y != operand.y || z != operand.z);
}

void Vector3::set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void Vector3::clear()
{
	x = y = z = 0;
}

float Vector3::getLength() const
{
	return Math::sqrt(x * x + y * y + z * z);
}

float Vector3::getLengthSquared() const
{
	return x * x + y * y + z * z;
}

float Vector3::getLengthManhattan() const
{
	return Math::abs(x) + Math::abs(y) + Math::abs(z);
}

void Vector3::normalize()
{
	if (getLengthManhattan() == 0)
	{
		return;
	}
	float inverseMagnitude = 1 / getLength();
	x *= inverseMagnitude;
	y *= inverseMagnitude;
	z *= inverseMagnitude;
}