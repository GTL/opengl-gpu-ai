#include <Cross/Input/Input.h>
#include <Cross/Exception.h>
#include <Cross/Window.h>
#include <Cross/Math/Vector3.h>
#include <string.h>
#include <iostream>

#if defined(_WIN32)
Cross::Input::Key Cross::Input::Key::a(DIK_A); Cross::Input::Key Cross::Input::Key::b(DIK_B);
Cross::Input::Key Cross::Input::Key::c(DIK_C); Cross::Input::Key Cross::Input::Key::d(DIK_D);
Cross::Input::Key Cross::Input::Key::e(DIK_E); Cross::Input::Key Cross::Input::Key::f(DIK_F);
Cross::Input::Key Cross::Input::Key::g(DIK_G); Cross::Input::Key Cross::Input::Key::h(DIK_H);
Cross::Input::Key Cross::Input::Key::i(DIK_I); Cross::Input::Key Cross::Input::Key::j(DIK_J);
Cross::Input::Key Cross::Input::Key::k(DIK_K); Cross::Input::Key Cross::Input::Key::l(DIK_L);
Cross::Input::Key Cross::Input::Key::m(DIK_M); Cross::Input::Key Cross::Input::Key::n(DIK_N);
Cross::Input::Key Cross::Input::Key::o(DIK_O); Cross::Input::Key Cross::Input::Key::p(DIK_P);
Cross::Input::Key Cross::Input::Key::q(DIK_Q); Cross::Input::Key Cross::Input::Key::r(DIK_R);
Cross::Input::Key Cross::Input::Key::s(DIK_S); Cross::Input::Key Cross::Input::Key::t(DIK_T);
Cross::Input::Key Cross::Input::Key::u(DIK_U); Cross::Input::Key Cross::Input::Key::v(DIK_V);
Cross::Input::Key Cross::Input::Key::w(DIK_W); Cross::Input::Key Cross::Input::Key::x(DIK_X);
Cross::Input::Key Cross::Input::Key::y(DIK_Y); Cross::Input::Key Cross::Input::Key::z(DIK_Z);
Cross::Input::Key Cross::Input::Key::space(DIK_SPACE);
Cross::Input::Key Cross::Input::Key::leftShift(DIK_LSHIFT);
Cross::Input::Key Cross::Input::Key::up(DIK_UP);
Cross::Input::Key Cross::Input::Key::down(DIK_DOWN);
Cross::Input::Key Cross::Input::Key::left(DIK_LEFT);
Cross::Input::Key Cross::Input::Key::right(DIK_RIGHT);
#elif defined(unix)
Cross::Input::Key Cross::Input::Key::a(38); Cross::Input::Key Cross::Input::Key::b(56);
Cross::Input::Key Cross::Input::Key::c(54); Cross::Input::Key Cross::Input::Key::d(40);
Cross::Input::Key Cross::Input::Key::e(26); Cross::Input::Key Cross::Input::Key::f(41);
Cross::Input::Key Cross::Input::Key::g(42); Cross::Input::Key Cross::Input::Key::h(43);
Cross::Input::Key Cross::Input::Key::i(31); Cross::Input::Key Cross::Input::Key::j(44);
Cross::Input::Key Cross::Input::Key::k(45); Cross::Input::Key Cross::Input::Key::l(46);
Cross::Input::Key Cross::Input::Key::m(58); Cross::Input::Key Cross::Input::Key::n(57);
Cross::Input::Key Cross::Input::Key::o(32); Cross::Input::Key Cross::Input::Key::p(33);
Cross::Input::Key Cross::Input::Key::q(24); Cross::Input::Key Cross::Input::Key::r(27);
Cross::Input::Key Cross::Input::Key::s(39); Cross::Input::Key Cross::Input::Key::t(28);
Cross::Input::Key Cross::Input::Key::u(30); Cross::Input::Key Cross::Input::Key::v(55);
Cross::Input::Key Cross::Input::Key::w(25); Cross::Input::Key Cross::Input::Key::x(53);
Cross::Input::Key Cross::Input::Key::y(29); Cross::Input::Key Cross::Input::Key::z(52);
Cross::Input::Key Cross::Input::Key::space(65);
Cross::Input::Key Cross::Input::Key::leftShift(50);
#endif

Cross::Input::Key::Key(UByte key)
{
	this->key = key;
}

void Cross::Input::calculateMousePosition()
{
#if defined(_WIN32)
	POINT mousePos;
	GetCursorPos(&mousePos);

	// account for window position and style
	RECT windowRect;
	RECT style;
	windowRect.bottom = windowRect.left = windowRect.right = windowRect.top = 0;
	style.bottom = style.left = style.right = style.top = 0;
	GetWindowRect(window->getHandle(), &windowRect);
	AdjustWindowRect(&style, window->getStyle(), false);
	mousePos.x -= (windowRect.left - style.left);
	mousePos.y -= (windowRect.top - style.top);
	mousePosition = Vector2((float)mousePos.x, (float)mousePos.y);
#elif defined(unix)
	prevMousePosition = mousePosition;
	int tempInt, mouseX, mouseY;
	::Window tempWindow;
	unsigned int tempUInt;
	XQueryPointer(window->getDisplay(), window->getWindow(), &tempWindow, &tempWindow, &tempInt, &tempInt, &mouseX, &mouseY, &tempUInt);
	mousePosition = Vector2(mouseX, mouseY);
#endif
}

Cross::Input::Input(Cross::Window* window) : window(window)
{
#if defined(_WIN32)
	HR(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&object, nullptr));

	HR(object->CreateDevice(GUID_SysKeyboard, &keyboardDevice, NULL));
	keyboardDevice->SetDataFormat(&c_dfDIKeyboard);
	keyboardDevice->SetCooperativeLevel(window->getHandle(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	HR(object->CreateDevice(GUID_SysMouse, &mouseDevice, NULL));
	mouseDevice->SetDataFormat(&c_dfDIMouse2);
	mouseDevice->SetCooperativeLevel(window->getHandle(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	calculateMousePosition();
#elif defined(unix)
	calculateMousePosition();
	prevMousePosition = mousePosition;

	memset(keyboardState, 0, 256 * sizeof(Bool));
	memset(prevKeyboardState, 0, 256 * sizeof(Bool));
#endif
}

Cross::Input::~Input()
{
}

bool Cross::Input::keyDown(Key key) const
{
#if defined(_WIN32)
	return (keyboardState[key.key] & 0x80) != 0;
#elif defined(unix)
	return keyboardState[key.key];
#endif
}

bool Cross::Input::keyPressed(Key key) const
{
#if defined(_WIN32)
	return ((keyboardState[key.key] & 0x80) != 0) && ((prevKeyboardState[key.key] & 0x80) == 0);
#elif defined(unix)
	return keyboardState[key.key] && !prevKeyboardState[key.key];
#endif
}

bool Cross::Input::keyReleased(Key key) const
{
#if defined(_WIN32)
	return (keyboardState[key.key] & 0x80) == 0 && (prevKeyboardState[key.key] & 0x80) != 0;
#elif defined(unix)
	return !keyboardState[key.key] && prevKeyboardState[key.key];
#endif
}

bool Cross::Input::mouseDown(UByte button) const
{
#if defined(_WIN32)
	return (mouseState.rgbButtons[button] & 0x80) != 0;
#elif defined(unix)
	return false;
#endif
}

bool Cross::Input::mousePressed(UByte button) const
{
#if defined(_WIN32)
	return (mouseState.rgbButtons[button] & 0x80) != 0 && (prevMouseState.rgbButtons[button] & 0x80) == 0;
#elif defined(unix)
	return false;
#endif
}

bool Cross::Input::mouseReleased(UByte button) const
{
#if defined(_WIN32)
	return (mouseState.rgbButtons[button] & 0x80) == 0 && (prevMouseState.rgbButtons[button] & 0x80) != 0;
#elif defined(unix)
	return false;
#endif
}

void Cross::Input::update()
{
#if defined(_WIN32)
	keyboardDevice->Acquire();
	mouseDevice->Acquire();

	memcpy(prevKeyboardState, keyboardState, 256);
	prevMouseState = mouseState;

	ZeroMemory(keyboardState, 256);
	HRESULT hr = keyboardDevice->GetDeviceState(sizeof(keyboardState), &keyboardState);

	ZeroMemory(&mouseState, sizeof(mouseState));
	hr = mouseDevice->GetDeviceState(sizeof(DIMOUSESTATE2), &mouseState);
	//mouseState = MouseState(mouse, Vector2((Real)mousePosition.x, (Real)mousePosition.y));
#elif defined(unix)
	memcpy(prevKeyboardState, keyboardState, 256);
	std::vector<XEvent> events = window->getEvents();
	for (unsigned int i = 0; i < events.size(); i++)
	{
		XEvent& event = events[i];
		bool first = (i == 0);
		bool last = (i == events.size() - 1);
		switch (event.type)
		{
		case KeyPress:
			if ((first || events[i - 1].type != KeyRelease) && (event.xbutton.button >= 0 && event.xbutton.button <= 255))
			{
				keyboardState[event.xbutton.button] = true;
			}
			break;
		case KeyRelease:
			if ((last || events[i + 1].type != KeyPress) && (event.xbutton.button >= 0 && event.xbutton.button <= 255))
			{
				keyboardState[event.xbutton.button] = false;
			}
			break;
		}
	}
#endif
	calculateMousePosition();
}

Vector3 Cross::Input::getMouseMove() const
{
#if defined(_WIN32)
	return Vector3((float)mouseState.lX, (float)mouseState.lY, (float)mouseState.lZ);
#elif defined(unix)
	return Vector3(mousePosition.x - prevMousePosition.x, mousePosition.y - prevMousePosition.y, 0);
#endif
}

Vector2 Cross::Input::getMousePosition() const
{
	return mousePosition;
}

void Cross::Input::setMousePosition(const Vector2& mousePosition)
{
#if defined(_WIN32)
	// account for window position and style
	RECT windowRect;
	RECT style;
	windowRect.bottom = windowRect.left = windowRect.right = windowRect.top = 0;
	style.bottom = style.left = style.right = style.top = 0;
	GetWindowRect(window->getHandle(), &windowRect);
	AdjustWindowRect(&style, window->getStyle(), false);

	SetCursorPos((int)(mousePosition.x + (windowRect.left - style.left)), (int)(mousePosition.y + (windowRect.top - style.top)));
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(window->getDisplay(), window->getWindow(), &attributes);
	XWarpPointer(window->getDisplay(), 0, window->getWindow(), 0, 0, 0, 0, attributes.x + (int)mousePosition.x, attributes.y + (int)mousePosition.y);
#endif
	calculateMousePosition();
}

void Cross::Input::setBackground(bool background)
{
	if (background)
	{
		keyboardDevice->SetCooperativeLevel(window->getHandle(), DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
		mouseDevice->SetCooperativeLevel(window->getHandle(), DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
	}
	else
	{
		keyboardDevice->SetCooperativeLevel(window->getHandle(), DISCL_FOREGROUND| DISCL_NONEXCLUSIVE);
		mouseDevice->SetCooperativeLevel(window->getHandle(), DISCL_FOREGROUND| DISCL_NONEXCLUSIVE);
	}
}