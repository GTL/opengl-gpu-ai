#include <Cross/Application.h>
#include <Cross/Window.h>
#include <Cross/Graphics/Graphics.h>
#include <Cross/Core.h>
#include <Cross/Utility/Time.h>
#include <Cross/Input/Input.h>

bool Cross::Application::running = true;

void Cross::Application::quit()
{
	running = false;
}

void Cross::Application::sleep(float milliseconds)
{
#if defined(_WIN32)
	Sleep((DWORD)milliseconds);
#elif defined(unix)
	usleep(milliseconds * 1000);
#endif
}

Cross::Application::Application()
{
#if defined(unix)
	glGenVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");
	glBindVertexArrayAPPLE = (void(*)(const GLuint))glXGetProcAddressARB((GLubyte*)"glBindVertexArray");
	glDeleteVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");
	glXCreateContextAttribsARB = (GLXContext(*)(Display* dpy, GLXFBConfig config, GLXContext share_context, Bool direct, const int *attrib_list))glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	glXChooseFBConfig = (GLXFBConfig*(*)(Display *dpy, int screen, const int *attrib_list, int *nelements))glXGetProcAddressARB((GLubyte*)"glXChooseFBConfig");
	glXGetVisualFromFBConfig = (XVisualInfo*(*)(Display *dpy, GLXFBConfig config))glXGetProcAddressARB((GLubyte*)"glXGetVisualFromFBConfig");
#endif

	window = nullptr;
	graphics = nullptr;
	input = nullptr;
}

Cross::Application::~Application()
{
}

void Cross::Application::initialize()
{
	Window::Class windowClass;
	windowClass.name = "CrossApp";
	windowClass.width = 800;
	windowClass.height = 600;
	windowClass.background = Color(0, 0, 0);
	window = new Window(windowClass);
	window->show();

	Graphics::Parameters parameters;
	parameters.window = window;
	parameters.width = window->getWidth();
	parameters.height = window->getHeight();
	graphics = new Graphics(parameters);

	window->focus();

	previousTime = Time::getCounter();

	input = new Input(window);
}

void Cross::Application::update(const float dt)
{
	input->update();
	window->update();
	if (window->isDestroyed())
	{
		quit();
	}
}

void Cross::Application::draw()
{
}

void Cross::Application::uninitialize()
{
	freeptr(window);
	freeptr(graphics);
	freeptr(input);
}

void Cross::Application::run()
{
	initialize();
	while (running)
	{
		sleep(1);
		float time = Time::getCounter();
		update(time - previousTime);
		previousTime = time;
		draw();
	}
	uninitialize();
}
