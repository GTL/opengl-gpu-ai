#include <Cross/Exception.h>

Exception::Exception(std::string message, std::string file, long line)
{
	this->message = message;
	this->file = file;
	this->line = line;
	result = 0;
}

Exception::Exception(std::string message, std::string file, long line, int result)
{
	this->message = message;
	this->file = file;
	this->line = line;
	this->result = result;
}
